package net.aminecraftdev.custombosses.zcore.i;

import net.aminecraftdev.custombosses.utils.IInventoryManager;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Created by Charles on 9/3/2017.
 */
public interface IPlugin {

    void refreshAllFields();

    void loadFiles();

    void reloadFiles();

    void saveFiles();

    FileConfiguration getAutoSpawnsYML();

    FileConfiguration getBossesYML();

    FileConfiguration getDropTableYML();

    FileConfiguration getLangYML();

    FileConfiguration getSkillsYML();

    FileConfiguration getConfigYML();

    FileConfiguration getItemsYML();

    IInventoryManager getInventoryManager();
}
