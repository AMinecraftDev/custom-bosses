package net.aminecraftdev.custombosses.zcore.i;

import net.aminecraftdev.custombosses.utils.*;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.utils.skill.ISkillUtils;

/**
 * Created by Charles on 8/3/2017.
 */
public interface ICompositeRoot {

    void reloadFields();

    IBossUtils getBossUtils();

    IAutoSpawnUtils getAutoSpawnUtils();

    IDropTableUtils getDropTableUtils();

    ISkillUtils getSkillUtils();

    ICommandUtils getCommandUtils();

    ICommandCore getCommandCore();

    IListenerCore getListenerCore();

    ILocationManager getLocationManager();

    IInventoryManager getInventoryManager();
}
