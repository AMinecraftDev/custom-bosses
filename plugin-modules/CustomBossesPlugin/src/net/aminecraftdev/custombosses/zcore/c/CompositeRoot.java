package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.commands.BossCmd;
import net.aminecraftdev.custombosses.commands.GiveEggCmd;
import net.aminecraftdev.custombosses.listeners.*;
import net.aminecraftdev.custombosses.listeners.boss.after.BossDeathCommandListener;
import net.aminecraftdev.custombosses.listeners.boss.during.*;
import net.aminecraftdev.custombosses.listeners.boss.after.BossDeathListener;
import net.aminecraftdev.custombosses.listeners.boss.pre.BossSpawnCommandListener;
import net.aminecraftdev.custombosses.listeners.boss.pre.BossSpawnListener;
import net.aminecraftdev.custombosses.listeners.minion.MinionDeathListener;
import net.aminecraftdev.custombosses.listeners.players.PlayerMoveListener;
import net.aminecraftdev.custombosses.utils.*;
import net.aminecraftdev.custombosses.utils.autospawn.AutoSpawn;
import net.aminecraftdev.custombosses.utils.IAutoSpawn;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.utils.placeholders.HologramsPlaceholder;
import net.aminecraftdev.custombosses.utils.placeholders.MVdWPlaceholders;
import net.aminecraftdev.custombosses.utils.skill.ISkillUtils;
import net.aminecraftdev.custombosses.zcore.i.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import java.util.*;
import java.util.logging.Level;

/**
 * Created by Charles on 8/3/2017.
 */
public class CompositeRoot implements ICompositeRoot {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private Random _random = new Random();
    private IInventoryManager _inventoryManager;
    private IBossUtils _bossUtils;
    private IAutoSpawnUtils _autoSpawnUtils;
    private IDropTableUtils _dropTableUtils;
    private ISkillUtils _skillUtils;
    private ICommandUtils _commandUtils;
    private ICommandCore _commandCore;
    private IListenerCore _listenerCore;
    private ILocationManager _locationManager;
    private INumberUtils _numberUtils;
    private IMessageUtils _messageUtils;
    private IItemStackUtils _itemStackUtils;
    private IPotionUtils _potionUtils;
    private IActionBarReflection _actionBarReflection;
    private IChunkReflection _chunkReflection;
    private IEntityUtils _entityUtils;
    private boolean _isHologramEnabled = false;
    private boolean _isMVdWPlaceholersEnabled = false;

    public CompositeRoot(INumberUtils numberUtils, IMessageUtils messageUtils, IItemStackUtils itemStackUtils, IPotionUtils potionUtils, IActionBarReflection actionBarReflection, IChunkReflection chunkReflection, IEntityUtils entityUtils) {
        _numberUtils = numberUtils;
        _messageUtils = messageUtils;
        _itemStackUtils = itemStackUtils;
        _potionUtils = potionUtils;
        _actionBarReflection = actionBarReflection;
        _chunkReflection = chunkReflection;
        _entityUtils = entityUtils;

        _skillUtils = new SkillUtils();
        _bossUtils = new BossUtils();
        _autoSpawnUtils = new AutoSpawnUtils();
        _dropTableUtils = new DropTableUtils();
        _commandUtils = new CommandUtils();
        _locationManager = new LocationManager();
        _inventoryManager = new InventoryManager();
        _commandCore = new CommandCore(_plugin);
        _listenerCore = new ListenerCore(_plugin);

        Plugin plugin = Bukkit.getPluginManager().getPlugin("HolographicDisplays");

        if(plugin != null) _isHologramEnabled = true;

        plugin = Bukkit.getPluginManager().getPlugin("MVdWPlaceholderAPI");

        if(plugin != null) _isMVdWPlaceholersEnabled = true;
    }

    @Override
    public void reloadFields() {
        getBossUtils().reloadSettings();
        getLocationManager().reloadSettings();
        getAutoSpawnUtils().refreshAutoSpawns();
        _inventoryManager = new InventoryManager();
    }

    @Override
    public IBossUtils getBossUtils() {
        return _bossUtils;
    }

    @Override
    public IAutoSpawnUtils getAutoSpawnUtils() {
        return _autoSpawnUtils;
    }

    @Override
    public IDropTableUtils getDropTableUtils() {
        return _dropTableUtils;
    }

    @Override
    public ISkillUtils getSkillUtils() {
        return _skillUtils;
    }

    @Override
    public ICommandUtils getCommandUtils() {
        return _commandUtils;
    }

    @Override
    public ICommandCore getCommandCore() {
        return _commandCore;
    }

    @Override
    public IListenerCore getListenerCore() {
        return _listenerCore;
    }

    @Override
    public ILocationManager getLocationManager() {
        return _locationManager;
    }

    @Override
    public IInventoryManager getInventoryManager() {
        return _inventoryManager;
    }

    /**
     * This class is the BossUtils but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class BossUtils implements IBossUtils {

        private List<LivingEntity> _listOfBosses = new ArrayList<LivingEntity>();
        private Map<UUID, String> _mapOfBossType = new HashMap<UUID, String>();
        private Map<UUID, String> _mapOfbossName = new HashMap<UUID, String>();
        private Map<UUID, List<LivingEntity>> _mapOfBossAndMinions = new HashMap<UUID, List<LivingEntity>>();
        private Map<UUID, List<String>> _mapOfDamages = new HashMap<UUID, List<String>>();
        private int _radiusOnSpawn, _radiusOnDeath, _amountOfPlayers, _actionBarRadius;

        public BossUtils() {
            reloadSettings();
        }

        @Override
        public void reloadSettings() {
            _radiusOnSpawn = _plugin.getConfigYML().getInt("Settings.BroadcastRange.onSpawn", -1);
            _radiusOnDeath = _plugin.getConfigYML().getInt("Settings.BroadcastRange.onDeath", -1);
            _amountOfPlayers = _plugin.getConfigYML().getInt("Settings.KilledMessage.amountOfPlayers", 5);
            _actionBarRadius = _plugin.getConfigYML().getInt("Settings.ActionBar.radius", 80);
        }

        @Override
        public String getBossType(UUID livingEntity) {
            return _mapOfBossType.get(livingEntity);
        }

        @Override
        public String getBossName(UUID livingEntity) {
            return _mapOfbossName.get(livingEntity);
        }

        @Override
        public List<String> getMapOfDamages(UUID livingEntity) {
            return _mapOfDamages.get(livingEntity);
        }

        @Override
        public boolean isLivingEntityBoss(LivingEntity livingEntity) {
            if(_listOfBosses.contains(livingEntity)) return true;
            return false;
        }

        @Override
        public boolean isLivingEntityMinion(LivingEntity livingEntity) {
            Iterator<UUID> iterator = _mapOfBossAndMinions.keySet().iterator();

            while(iterator.hasNext()) {
                UUID bossEntity = iterator.next();
                List<LivingEntity> listOfMinions = _mapOfBossAndMinions.get(bossEntity);

                if(listOfMinions.contains(livingEntity)) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public Object[] getTopPlayerArray(List<String> totalDamages) {
            Map<String, Double> totalPercentageCount = new HashMap<String, Double>();
            List<String> damagerNames = new ArrayList<String>();
            double totalDamage = 0.0D;

            for(int i = 0; i < totalDamages.size(); i++) {
                String[] split = totalDamages.get(i).split(":");
                double damage = Double.valueOf(split[1]);

                totalDamage += damage;
            }

            double onePercent = totalDamage / 100;

            for(int i = 0; i < totalDamages.size(); i++) {
                String[] split = totalDamages.get(i).split(":");
                double damage = Double.valueOf(split[1]);
                String player = split[0];

                if(Bukkit.getPlayer(player) == null) continue;

                double playerPercent = damage / onePercent;

                totalPercentageCount.put(player, playerPercent);
                damagerNames.add(player);
            }

            Object[] a = totalPercentageCount.entrySet().toArray();

            Arrays.sort(a, new Comparator() {

                @Override
                public int compare(Object o1, Object o2) {
                    return ((Map.Entry<String, Double>) o2).getValue().compareTo(
                            ((Map.Entry<String, Double>) o1).getValue());
                }

            });

            return a;
        }

        @Override
        public String getLocationString(Location location) {
            return location.getBlockX() + ", " + location.getBlockY() + ", " +location.getBlockZ();
        }

        @Override
        public Map<Integer, Player> getMapOfBossKillers(UUID livingEntity) {
            List<String> totalDamages = _mapOfDamages.get(livingEntity);
            Object[] topPlayerArray = getTopPlayerArray(totalDamages);
            int position = 1;
            Map<Integer, Player> map = new HashMap<Integer, Player>();

            for(Object object : topPlayerArray) {
                String player = ((Map.Entry<String, Double>) object).getKey();

                if(Bukkit.getPlayer(player) == null) continue;
                map.put(position, Bukkit.getPlayer(player));
                position++;
            }

            return map;
        }

        @Override
        public double getPlayerDamage(String player, UUID livingEntity) {
            List<String> totalDamages = _mapOfDamages.get(livingEntity);
            Object[] topPlayerArray = getTopPlayerArray(totalDamages);

            for(Object object : topPlayerArray) {
                String pName = ((Map.Entry<String, Double>) object).getKey();
                double percent = ((Map.Entry<String, Double>) object).getValue();

                if(pName.equals(player)) {
                    return percent;
                }
            }

            return 0.0D;
        }

        @Override
        public List<String> getSpawnMessages(List<String> messages, String location, String type) {
            List<String> spawnMessage = new ArrayList<String>();

            for(String s : messages) {
                if(s.contains("{boss}")) s = s.replace("{boss}", type);
                if(s.contains("{loc}")) s = s.replace("{loc}", location);

                spawnMessage.add(_messageUtils.translateString(s));
            }

            return spawnMessage;
        }

        @Override
        public List<String> getDeathMessage(List<String> totalDamages, UUID uuid, LivingEntity livingEntity) {
            String bossKey = _mapOfBossType.get(uuid);
            String perPlayerFormat = _plugin.getBossesYML().getString("Bosses." + bossKey + ".Messages.onDeathFormat");
            List<String> onDeathMessages = _plugin.getBossesYML().getStringList("Bosses." + bossKey + ".Messages.onDeath");
            int messageLength = 0;
            Object[] topPlayerArray = getTopPlayerArray(totalDamages);
            List<String> customArray = new ArrayList<String>();
            String currentLine = "";

            for(Object x : topPlayerArray) {
                if(messageLength == _amountOfPlayers) break;

                double percent = ((Map.Entry<String, Double>) x).getValue();
                String player = ((Map.Entry<String, Double>) x).getKey();

                if(player == null) continue;

                currentLine += perPlayerFormat
                        .replace("{player}", player)
                        .replace("{%}", _numberUtils.formatDouble(percent))
                        .replace("{position}", ""+(messageLength+1));

                if(currentLine.contains("/n")) {
                    String[] split = currentLine.split("/n");

                    for(String s : split) {
                        if(s == "" || s == " ") continue;

                        customArray.add(_messageUtils.translateString(s));
                    }

                    currentLine = "";
                }

                messageLength += 1;
            }

            if(currentLine != "" || currentLine != " ") {
                customArray.add(_messageUtils.translateString(currentLine));
            }

            String name = _mapOfbossName.containsKey(uuid)? _mapOfbossName.get(uuid):livingEntity.getCustomName();
            List<String> finalListOfMessage = new ArrayList<String>();

            for(String s : onDeathMessages) {
                if(s.contains("{boss}")) s = s.replace("{boss}", name);

                if(s.contains("{format}")) {
                    s = s.replace("{format}", "");
                    finalListOfMessage.add(_messageUtils.translateString(s));
                    finalListOfMessage.addAll(customArray);
                } else {
                    finalListOfMessage.add(_messageUtils.translateString(s));
                }
            }

            return finalListOfMessage;
        }

        @Override
        public void onSpawnBroadcastMessage(Location location, String type, World world) {
            int radiusSqr = _radiusOnSpawn*_radiusOnSpawn;
            String sLoc = getLocationString(location);
            ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses." + type);
            List<String> messages = configurationSection.getStringList("Messages.onSpawn");
            List<String> arrayOfMessages = getSpawnMessages(messages, sLoc, type);

            for(Player player : Bukkit.getOnlinePlayers()) {
                if(_radiusOnSpawn == -1) {
                    _messageUtils.sendMessages(player, arrayOfMessages);
                } else {
                    if(player.getWorld().equals(world) && player.getLocation().distanceSquared(location) <= radiusSqr) {
                        _messageUtils.sendMessages(player, arrayOfMessages);
                    }
                }
            }
        }

        @Override
        public void onDeathBroadcastMessage(Location location, List<String> totalDamages, UUID uuid, World world, LivingEntity livingEntity) {
            int radiusSqr = _radiusOnDeath*_radiusOnDeath;
            List<String> arrayOfMessages = getDeathMessage(totalDamages, uuid, livingEntity);

            for(Player player : Bukkit.getOnlinePlayers()) {
                if(_radiusOnDeath == -1) {
                    _messageUtils.sendMessages(player, arrayOfMessages);
                } else {
                    if(player.getWorld().equals(world) && player.getLocation().distanceSquared(location) <= radiusSqr) {
                        _messageUtils.sendMessages(player, arrayOfMessages);
                    }
                }
            }
        }

        @Override
        public LivingEntity spawnLivingEntity(Location location, ConfigurationSection configurationSection) {
            LivingEntity boss;
            String bossType = configurationSection.getString("Boss.type");
            EntityType entityType;

            if(bossType.equalsIgnoreCase("WITHER_SKELETON")) {
                boss = _entityUtils.getWitherSkeleton(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("ELDER_GUARDIAN")) {
                boss = _entityUtils.getElderGuardian(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("KILLER_BUNNY")) {
                boss = _entityUtils.getKillerBunny(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("ZOMBIE")) {
                boss = _entityUtils.getZombie(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("BABY_ZOMBIE")) {
                boss = _entityUtils.getBabyZombie(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("PIG_ZOMBIE")) {
                boss = _entityUtils.getPigZombie(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("BABY_PIG_ZOMBIE")) {
                boss = _entityUtils.getBabyPigZombie(location);
                entityType = null;
            } else if(bossType.equalsIgnoreCase("CHARGED_CREEPER")) {
                boss = _entityUtils.getChargedCreeper(location);
                entityType = null;
            } else if(bossType.contains(":")) {
                String[] split = bossType.split(":");

                if(split[0].equalsIgnoreCase("SLIME")) {
                    boss = _entityUtils.getSlime(location, Integer.valueOf(split[1]));
                    entityType = null;
                } else if(split[0].equalsIgnoreCase("MAGMA_CUBE")) {
                    boss = _entityUtils.getMagmaCube(location, Integer.valueOf(split[1]));
                    entityType = null;
                } else {
                    if(EntityType.valueOf(bossType) == null) {
                        Debug.Boss_InvalidEntityType.out(bossType);
                        return null;
                    }

                    entityType = EntityType.valueOf(bossType);
                    boss = null;
                }
            } else {
                if(EntityType.valueOf(bossType) == null) {
                    Debug.Boss_InvalidEntityType.out(bossType);
                    return null;
                }

                entityType = EntityType.valueOf(bossType);
                boss = null;
            }

            if(boss == null) {
                if(entityType == EntityType.SLIME || entityType == EntityType.MAGMA_CUBE) {
                    Slime slime = (Slime) location.getWorld().spawn(location, entityType.getEntityClass());
                    slime.setSize(10);

                    boss = slime;
                }  else {
                    boss = (LivingEntity) location.getWorld().spawn(location, entityType.getEntityClass());
                }
            }


            if(configurationSection.getString("Boss.name") == null) {
                Debug.Boss_NullName.out();
                return null;
            }

            String name = _messageUtils.translateString(configurationSection.getString("Boss.name"));
            double health = configurationSection.getDouble("Boss.health");

            if(health > 2000) {
                Debug.Boss_OverMaxHealth.out(health);
                return null;
            }

            boss.setCustomName(name);
            boss.setMaxHealth(health);
            boss.setHealth(health);
            boss.setRemoveWhenFarAway(false);
            boss.setCanPickupItems(false);

            applyEquipmentToBoss(boss, "Armor", configurationSection);
            applyEquipmentToBoss(boss, "Weapon", configurationSection);

            if(!configurationSection.isSet("Boss.Potions")) return boss;

            ConfigurationSection potionConfigSection = configurationSection.getConfigurationSection("Boss.Potions");

            for(String s : potionConfigSection.getKeys(false)) {
                PotionEffect potionEffect = _potionUtils.getPotionEffect(potionConfigSection.getConfigurationSection(s));

                if(potionEffect == null) continue;

                boss.addPotionEffect(potionEffect);
            }

            boss.getEquipment().setHelmetDropChance(0.0F);
            boss.getEquipment().setChestplateDropChance(0.0F);
            boss.getEquipment().setLeggingsDropChance(0.0F);
            boss.getEquipment().setBootsDropChance(0.0F);
            boss.getEquipment().setItemInHandDropChance(0.0F);

            if(configurationSection.contains("Boss.Head") && configurationSection.getBoolean("Boss.Head.enabled", false)) {
                String owner = configurationSection.getString("Boss.Head.owner");
                ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
                SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();

                skullMeta.setOwner(owner);
                itemStack.setItemMeta(skullMeta);

                boss.getEquipment().setHelmet(itemStack);
            }

            return boss;
        }

        @Override
        public LivingEntity spawnBoss(Location location, String type, ConfigurationSection configurationSection) {
            LivingEntity livingEntity = spawnLivingEntity(location, configurationSection);
            String name = _messageUtils.translateString(configurationSection.getString("Boss.name"));

            if(type == null || livingEntity == null) return null;

            _listOfBosses.add(livingEntity);
            _mapOfBossType.put(livingEntity.getUniqueId(), type);
            _mapOfbossName.put(livingEntity.getUniqueId(), name);
            _mapOfBossAndMinions.put(livingEntity.getUniqueId(), new ArrayList<LivingEntity>());

            return livingEntity;
        }

        @Override
        public LivingEntity spawnAutoBoss(Location location, String type, ConfigurationSection configurationSection, String timedSpawnSection) {
            LivingEntity livingEntity = spawnBoss(location, type, configurationSection);

            List<LivingEntity> listOfBossesInSection = getAutoSpawnUtils().getMapOfAutoBosses().containsKey(timedSpawnSection) ? getAutoSpawnUtils().getMapOfAutoBosses().get(timedSpawnSection) : new ArrayList<LivingEntity>();
            int currentlySpawned = _autoSpawnUtils.getMapOfAutoSpawnAmounts().containsKey(timedSpawnSection) ? _autoSpawnUtils.getMapOfAutoSpawnAmounts().get(timedSpawnSection) : 0;

            listOfBossesInSection.add(livingEntity);
            getAutoSpawnUtils().getListOfAutoBosses().add(livingEntity);
            getAutoSpawnUtils().getMapOfAutoBosses().put(timedSpawnSection, listOfBossesInSection);
            getAutoSpawnUtils().getMapOfAutoSpawnAmounts().put(timedSpawnSection, currentlySpawned+1);

            return livingEntity;
        }

        @Override
        public LivingEntity spawnMinion(Location location, ConfigurationSection configurationSection, LivingEntity target, UUID boss) {
            double closest = Double.MAX_VALUE;
            Player player = null;

            for(Player i : Bukkit.getWorld(location.getWorld().getName()).getPlayers()) {
                if(!i.getWorld().getName().equals(location.getWorld().getName())) continue;

                double dist = i.getLocation().distanceSquared(location);

                if(closest == Double.MAX_VALUE || dist < closest) {
                    closest = dist;
                    player = i;
                }
            }

            LivingEntity livingEntity = spawnLivingEntity(location, configurationSection);
            Creature creature = (Creature) livingEntity;

            if(player == null) {
                creature.setTarget(target);
            } else {
                creature.setTarget(player);
            }

            List<LivingEntity> listOfMinions = _mapOfBossAndMinions.get(boss);

            listOfMinions.add(livingEntity);
            _mapOfBossAndMinions.put(boss, listOfMinions);

            return livingEntity;
        }

        @Override
        public Map<Enchantment, Integer> getMapOfEnchants(ConfigurationSection configurationSection, String type) {
            Map<Enchantment, Integer> map = new HashMap<Enchantment, Integer>();
            List<String> configList = configurationSection.getStringList("Boss." + type + ".enchants");

            if(!configurationSection.getBoolean("Boss." + type + ".enabled", true)) return map;

            for(String s : configList) {
                String[] spl = s.split(":");
                String ench = spl[0];
                int level = Integer.parseInt(spl[1]);

                map.put(Enchantment.getByName(ench), level);
            }

            return map;
        }

        @Override
        public ItemStack getArmorItemStack(String type, Map<Enchantment, Integer> enchants) {
            ItemStack itemStack = new ItemStack(_itemStackUtils.getType(type));
            itemStack.addUnsafeEnchantments(enchants);

            return itemStack;
        }

        @Override
        public void applyEquipmentToBoss(LivingEntity boss, String type, ConfigurationSection configurationSection) {
            if(!configurationSection.contains("Boss." + type)) return;
            if(!configurationSection.contains("Boss." + type + ".enabled")) return;
            if(!configurationSection.contains("Boss." + type + ".type")) return;
            if(!configurationSection.getBoolean("Boss." + type + ".enabled", false)) return;

            Map<Enchantment, Integer> mapOfEnchants = getMapOfEnchants(configurationSection, type);
            String wType = configurationSection.getString("Boss." + type + ".type").toUpperCase();
            ItemStack equipment;

            if(type.equals("Armor")) {
                String innerType = wType + "_HELMET";

                equipment = getArmorItemStack(innerType, mapOfEnchants);
                boss.getEquipment().setHelmet(equipment);

                innerType = wType + "_CHESTPLATE";
                equipment = getArmorItemStack(innerType, mapOfEnchants);
                boss.getEquipment().setChestplate(equipment);

                innerType = wType + "_LEGGINGS";
                equipment = getArmorItemStack(innerType, mapOfEnchants);
                boss.getEquipment().setLeggings(equipment);

                innerType = wType + "_BOOTS";
                equipment = getArmorItemStack(innerType, mapOfEnchants);
                boss.getEquipment().setBoots(equipment);

            } else {
                equipment = new ItemStack(_itemStackUtils.getType(wType));

                try {
                    equipment.addUnsafeEnchantments(mapOfEnchants);
                } catch (IllegalArgumentException ex) {
                    Bukkit.getLogger().log(Level.WARNING, "Boss '" + type + "' had an empty enchant list.");
                }
                equipment.addUnsafeEnchantments(mapOfEnchants);
                boss.getEquipment().setItemInHand(equipment);
            }
        }

        @Override
        public void removeBoss(LivingEntity livingEntity) {
            _listOfBosses.remove(livingEntity);
            _mapOfBossType.remove(livingEntity.getUniqueId());
            _mapOfbossName.remove(livingEntity.getUniqueId());

            if(_mapOfBossAndMinions.get(livingEntity.getUniqueId()) != null) {
                for(LivingEntity minionEntity : _mapOfBossAndMinions.get(livingEntity.getUniqueId())) {
                    minionEntity.remove();
                }
            }

            _mapOfBossAndMinions.remove(livingEntity.getUniqueId());
            livingEntity.remove();
        }

        @Override
        public void removeAutoBoss(LivingEntity livingEntity) {
            removeBoss(livingEntity);

            _autoSpawnUtils.getListOfAutoBosses().remove(livingEntity);

            for(String key : _autoSpawnUtils.getMapOfAutoBosses().keySet()) {
                List<LivingEntity> listOfBosses = _autoSpawnUtils.getMapOfAutoBosses().get(key);

                if(!listOfBosses.contains(livingEntity)) continue;
                listOfBosses.remove(livingEntity);
                _autoSpawnUtils.getMapOfAutoBosses().put(key, listOfBosses);
                _autoSpawnUtils.getMapOfAutoSpawnAmounts().put(key, _autoSpawnUtils.getMapOfAutoSpawnAmounts().get(key) - 1);
            }
        }

        @Override
        public int getMinionCount(LivingEntity livingEntity) {
            return _mapOfBossAndMinions.containsKey(livingEntity.getUniqueId())? _mapOfBossAndMinions.get(livingEntity.getUniqueId()).size() : 0;
        }

        @Override
        public void removeMinion(LivingEntity livingEntity) {
            Iterator<UUID> iterator = _mapOfBossAndMinions.keySet().iterator();

            while(iterator.hasNext()) {
                UUID bossEntity = iterator.next();
                List<LivingEntity> listOfMinions = _mapOfBossAndMinions.get(bossEntity);

                if(listOfMinions.contains(livingEntity)) {
                    listOfMinions.remove(livingEntity);
                    _mapOfBossAndMinions.put(bossEntity, listOfMinions);
                }
            }
        }

        @Override
        public int removeAllMinions() {
            return removeAllMinions(null);
        }

        @Override
        public int removeAllMinions(World world) {
            int amount = 0;
            Iterator<UUID> iterator = _mapOfBossAndMinions.keySet().iterator();

            while(iterator.hasNext()) {
                UUID bossEntity = iterator.next();
                List<LivingEntity> listOfMinions = _mapOfBossAndMinions.get(bossEntity);
                Iterator<LivingEntity> minionIterator = listOfMinions.iterator();

                while(minionIterator.hasNext()) {
                    LivingEntity minion = minionIterator.next();

                    if((world == null) || (minion != null && minion.getWorld().equals(world))) {
                        minion.remove();
                        minionIterator.remove();
                        amount++;
                    }
                }
            }

            return amount;
        }

        @Override
        public int removeAllBosses() {
            Iterator<LivingEntity> iterator = _listOfBosses.iterator();
            int amount = 0;

            while(iterator.hasNext()) {
                LivingEntity livingEntity = iterator.next();

                iterator.remove();

                if(_autoSpawnUtils.isLivingEntityAutoBoss(livingEntity)) {
                    removeAutoBoss(livingEntity);
                } else {
                    removeBoss(livingEntity);
                }
                amount++;
            }

            return amount;
        }

        @Override
        public int removeAllBosses(World world) {
            Iterator<LivingEntity> iterator = _listOfBosses.iterator();
            int amount = 0;

            while(iterator.hasNext()) {
                LivingEntity livingEntity = iterator.next();

                if((world == null) || (livingEntity != null && livingEntity.getWorld().equals(world))) {
                    iterator.remove();

                    if(_autoSpawnUtils.isLivingEntityAutoBoss(livingEntity)) {
                        removeAutoBoss(livingEntity);
                    } else {
                        removeBoss(livingEntity);
                    }
                    amount++;
                }
            }

            return amount;
        }

        @Override
        public void addDamageToEntity(UUID livingEntity, Player damager, double damage) {
            List<String> array = new ArrayList<String>();

            if(_mapOfDamages.containsKey(livingEntity)) {
                List<String> list = _mapOfDamages.get(livingEntity);
                List<String> players = new ArrayList<String>();
                double totalDamage;

                for(String s : list) {
                    String[] divide = s.split(":");

                    if(divide[0].equalsIgnoreCase(damager.getName())) {
                        totalDamage = Double.parseDouble(divide[1]);
                        totalDamage += damage;

                        array.add(damager.getName() + ":" + totalDamage);
                        players.add(damager.getName());
                    } else {
                        String[] values = s.split(":");

                        players.add(values[0]);
                        array.add(s);
                    }
                }

                if(!players.contains(damager.getName())) {
                    array.add(damager.getName() + ":" + damage);
                }
            } else {
                array.add(damager.getName() + ":" + damage);
            }

            _mapOfDamages.put(livingEntity, array);
        }

        @Override
        public LivingEntity getClosestBoss(Location location) {
            double closestDistance = _actionBarRadius * _actionBarRadius;
            LivingEntity closestBoss = null;
            Iterator<LivingEntity> iterator = _listOfBosses.iterator();

            while(iterator.hasNext()) {
                LivingEntity livingEntity = iterator.next();

                if(livingEntity == null) continue;
                if(livingEntity.getWorld() == null) continue;
                if(location.getWorld() == null) continue;
                if(!livingEntity.getWorld().getName().equals(location.getWorld().getName())) continue;
                double distance = location.distanceSquared(livingEntity.getLocation());

                if(distance > closestDistance) continue;

                closestBoss = livingEntity;
                closestDistance = distance;
            }

            return closestBoss;
        }
    }

    /**
     * This class is the AutoSpawnUtils but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class AutoSpawnUtils implements IAutoSpawnUtils {

        private boolean enabled = true;
        private List<String> _listOfAutoSpawnHologramPlaceholders = new ArrayList<String>();
        private List<LivingEntity> _listOfAutoBosses = new ArrayList<LivingEntity>();
        private Map<String, List<LivingEntity>> _mapOfAutoBosses = new HashMap<String, List<LivingEntity>>();
        private Map<String, Integer> _mapOfAutoSpawnAmounts = new HashMap<String, Integer>();
        private Map<String, IAutoSpawn> _mapOfAutoSpawns = new HashMap<String, IAutoSpawn>();

        @Override
        public void refreshAutoSpawns() {
            Iterator<String> iterator = _mapOfAutoSpawns.keySet().iterator();

            while(iterator.hasNext()) {
                String s = iterator.next();
                IAutoSpawn autoSpawn = _mapOfAutoSpawns.get(s);

                autoSpawn.getTimerTask().cancel();
                iterator.remove();
                continue;
            }

            _mapOfAutoSpawnAmounts.clear();

            if(_isHologramEnabled) {
                for(String s : getListOfActivatedHologramPlaceholders()) {
                    com.gmail.filoghost.holographicdisplays.api.HologramsAPI.unregisterPlaceholder(CustomBosses.getInstance(), s);
                }
            }

            loadAutoSpawns();
        }

        @Override
        public void loadAutoSpawns() {
            if(!enabled) return;

            if(!_plugin.getAutoSpawnsYML().contains("Settings.enabled")) {
                _plugin.getAutoSpawnsYML().set("Settings.enabled", true);
                _plugin.saveFiles();
                _plugin.reloadFiles();
            }

            enabled = _plugin.getAutoSpawnsYML().getBoolean("Settings.enabled", true);
            ConfigurationSection configurationSection = _plugin.getAutoSpawnsYML().getConfigurationSection("AutoSpawns");

            if(!enabled) {
                Debug.AutoSpawn_Disabled.out();
                return;
            }

            Debug.AutoSpawn_LoadingPlaceholders.out();

            for(String key : configurationSection.getKeys(false)) {
                ConfigurationSection innerSection = configurationSection.getConfigurationSection(key);
                AutoSpawn autoSpawn = new AutoSpawn(innerSection, getAutoSpawnUtils(), _chunkReflection, key, getBossUtils(), _messageUtils);

                autoSpawn.loadAllStats();
                autoSpawn.start();
                _mapOfAutoSpawns.put(key, autoSpawn);

                if(_isHologramEnabled) {
                    new HologramsPlaceholder(innerSection, key, _autoSpawnUtils, _numberUtils);
                }

                if(_isMVdWPlaceholersEnabled) {
                    new MVdWPlaceholders(innerSection, key, _autoSpawnUtils, _numberUtils);
                }
            }
        }

        @Override
        public List<String> getListOfActivatedHologramPlaceholders() {
            return _listOfAutoSpawnHologramPlaceholders;
        }

        @Override
        public Map<String, IAutoSpawn> getMapOfAutoSpawns() {
            return _mapOfAutoSpawns;
        }

        @Override
        public List<LivingEntity> getListOfAutoBosses() {
            return _listOfAutoBosses;
        }

        @Override
        public Map<String, List<LivingEntity>> getMapOfAutoBosses() {
            return _mapOfAutoBosses;
        }

        @Override
        public Map<String, Integer> getMapOfAutoSpawnAmounts() {
            return _mapOfAutoSpawnAmounts;
        }

        @Override
        public boolean isLivingEntityAutoBoss(LivingEntity livingEntity) {
            return _listOfAutoBosses.contains(livingEntity);
        }

        @Override
        public Location getLocation(ConfigurationSection configurationSection) {
            String s = configurationSection.getString("world");
            int x = configurationSection.getInt("x");
            int y = configurationSection.getInt("y");
            int z = configurationSection.getInt("z");
            String section = configurationSection.getParent().getName();

            if(s == null || Bukkit.getWorld(s) == null) {
                Debug.AutoSpawn_WorldNull.out(section);
                return null;
            }

            return new Location(Bukkit.getWorld(s), x, y, z);
        }

        @Override
        public int getSpawnRate(ConfigurationSection configurationSection) {
            String s = configurationSection.getString("Settings.spawnRate");
            int amount;

            if(s.contains("s")) {
                s = s.replace("s", "");
                amount = Integer.valueOf(s);

            } else if(s.contains("m")) {
                s = s.replace("m", "");
                amount = Integer.valueOf(s) * 60;

            } else if(s.contains("h")) {
                s = s.replace("h", "");
                amount = (Integer.valueOf(s) * 60) * 60;

            } else {
                if(!_numberUtils.isStringInteger(s)) {
                    Debug.AutoSpawn_InvalidSpawnRate.out(configurationSection.getName());
                    amount = Integer.MAX_VALUE;
                } else {
                    amount = Integer.valueOf(s);
                }
            }

            return amount;
        }

        @Override
        public int getRandomSpawnTime(int duration, String spawnType) {
            if(spawnType.equalsIgnoreCase("RANDOM")) {
                return _random.nextInt(duration);
            }

            return duration;
        }

        @Override
        public boolean canSpawn(Location location, int maxAmount, int spawnAmount, String sectionName, boolean spawnIfChunkNotLoaded) {
            int current = 0;

            if(getMapOfAutoSpawnAmounts().containsKey(sectionName)) {
                current = getMapOfAutoSpawnAmounts().get(sectionName);
            }

            if(current >= maxAmount) return false;
            if((current + spawnAmount) > maxAmount) return false;
            if(!_chunkReflection.isChunkLoaded(location)) {

                if(!spawnIfChunkNotLoaded) return false;
            }

            return true;
        }

        @Override
        public boolean isValidBoss(String boss) {
            if(_plugin.getBossesYML().contains("Bosses." + boss)) return true;
            return false;
        }
    }

    /**
     * This class is the DropTableUtils but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class DropTableUtils implements IDropTableUtils {

        @Override
        public void handleDropTable(String dropTable, UUID uuid) {
            Map<Integer, Player> mapOfPositions = getBossUtils().getMapOfBossKillers(uuid);

            if(!_plugin.getDropTableYML().contains(dropTable)) {
                Debug.DropTable_DoesntContain.out(dropTable);
                return;
            }

            ConfigurationSection damagePositions = _plugin.getDropTableYML().getConfigurationSection(dropTable);

            for(String s : damagePositions.getKeys(false)) {
                if(!_numberUtils.isStringInteger(s)) {
                    Debug.DropTable_InvalidDamagePosition.out(s);
                    continue;
                }

                int position = Integer.valueOf(s);
                ConfigurationSection configurationSection = damagePositions.getConfigurationSection(s);

                if(!mapOfPositions.containsKey(position)) continue;

                Player player = mapOfPositions.get(position);
                boolean randomCommand = configurationSection.getBoolean("RandomCommand");
                boolean randomCustomDrop = configurationSection.getBoolean("RandomCustomDrop");
                double requiredPercentage = configurationSection.getDouble("RequiredPercent", 0.0);
                double playerPercentage = _bossUtils.getPlayerDamage(player.getName(), uuid);
                List<ItemStack> allCustomDrops = randomCustomDrop? getRandomBossCustomDrops(configurationSection) : getBossCustomDrops(configurationSection);
                List<String> allCommands = randomCommand? getRandomBossCommands(configurationSection, player) : getBossCommands(configurationSection, player);

                if(requiredPercentage > playerPercentage) {
                    Messages.PERCENTAGETOOLOW.msg(player);
                    continue;
                }

                for(String command : allCommands) {
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
                }

                for(ItemStack itemStack : allCustomDrops) {
                    if(itemStack == null) continue;

                    System.out.println("#4 - " + itemStack.getAmount());

                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), itemStack);
                }
            }
        }

        @Override
        public List<ItemStack> getRandomBossCustomDrops(ConfigurationSection configurationSection) {
            List<ItemStack> itemStackList = new ArrayList<ItemStack>();
            int maxDrops = configurationSection.contains("MaxCustomDrops")? configurationSection.getInt("MaxCustomDrops") : 1000;
            List<String> customDropStrings = configurationSection.getStringList("customDrops");
            int amount = 0;

            while(amount < maxDrops) {
                int random = _random.nextInt(customDropStrings.size());
                String s = customDropStrings.get(random);

                ItemStack itemStack = null;
                String[] arrayOfStrings = s.split(" ");
                boolean hasChanceToApply = true;

                for(int i = 0; i < arrayOfStrings.length; i++) {
                    String t = arrayOfStrings[i];

                    if(t == null) continue;

                    if(t.contains("Chance:")) {
                        t = t.replace("Chance:", "");

                        if(!_numberUtils.isStringInteger(t)) {
                            Debug.DropTable_InvalidChance.out(t);
                            continue;
                        }

                        int chance = Integer.valueOf(t);
                        int randomNumber = _random.nextInt(100);

                        if(randomNumber > chance) {
                            hasChanceToApply = false;
                            continue;
                        }
                    }

                    if(!t.contains("Item:") || itemStack != null) continue;

                    t = t.replace("Item:", "");

                    if(!_plugin.getItemsYML().contains(t)) continue;

                    ConfigurationSection itemSection = _plugin.getItemsYML().getConfigurationSection(t);

                    itemStack = _itemStackUtils.createItemStackAndAmount(itemSection);
                }

                if(itemStack == null || itemStack.getType() == Material.AIR || !hasChanceToApply) continue;

                itemStackList.add(itemStack);
                amount++;
            }

            return itemStackList;
        }

        @Override
        public List<ItemStack> getBossCustomDrops(ConfigurationSection configurationSection) {
            List<ItemStack> itemStackList = new ArrayList<ItemStack>();
            int maxDrops = configurationSection.contains("MaxCustomDrops")? configurationSection.getInt("MaxCustomDrops") : 1000;
            List<String> customDropStrings = configurationSection.getStringList("customDrops");
            int amount = 0;

            for(String s : customDropStrings) {
                if(amount >= maxDrops) break;

                ItemStack itemStack = null;
                String[] arrayOfStrings = s.split(" ");
                boolean hasChanceToApply = true;

                for(int i = 0; i < arrayOfStrings.length; i++) {
                    String t = arrayOfStrings[i];

                    if(t.contains("Chance:")) {
                        t = t.replace("Chance:", "");

                        if(!_numberUtils.isStringInteger(t)) {
                            Debug.DropTable_InvalidChance.out(t);
                            continue;
                        }

                        int chance = Integer.valueOf(t);
                        int randomNumber = _random.nextInt(100);

                        if(randomNumber > chance) {
                            hasChanceToApply = false;
                            continue;
                        }
                    }

                    if(!t.contains("Item:") || itemStack != null) continue;
                    t = t.replace("Item:", "");

                    if(!_plugin.getItemsYML().contains(t)) continue;

                    itemStack = _itemStackUtils.createItemStackAndAmount(_plugin.getItemsYML().getConfigurationSection(t));
                }

                if(itemStack == null || itemStack.getType() == Material.AIR || !hasChanceToApply) continue;

                itemStackList.add(itemStack);
                amount++;
            }

            return itemStackList;
        }

        @Override
        public List<String> getRandomBossCommands(ConfigurationSection configurationSection, Player player) {
            List<String> commandList = new ArrayList<String>();
            int maxCommands = configurationSection.contains("MaxCommands")? configurationSection.getInt("MaxCommands") : 1000;
            List<String> customCommands = configurationSection.getStringList("commands");
            int amount = 0;

            while(amount < maxCommands) {
                int random = _random.nextInt(customCommands.size());
                String s = customCommands.get(random);
                String cmd;

                if(s.contains(": ")) {
                    String[] split = s.split(": ");

                    if(!_numberUtils.isStringInteger(split[0])) {
                        Debug.DropTable_InvalidChance.out(split[0]);
                        continue;
                    }

                    int chance = Integer.valueOf(split[0]);
                    int randomNumber = _random.nextInt(100);

                    if(randomNumber > chance) continue;

                    cmd = split[1];
                } else {
                    cmd = s;
                }

                cmd = cmd.replace("%player%", player.getName());
                cmd = cmd.replace("%Player%", player.getName());

                commandList.add(cmd);
                amount++;
            }

            return commandList;
        }

        @Override
        public List<String> getBossCommands(ConfigurationSection configurationSection, Player player) {
            List<String> commandList = new ArrayList<String>();
            int maxCommands = configurationSection.contains("MaxCommands")? configurationSection.getInt("MaxCommands") : 1000;
            List<String> customCommands = configurationSection.getStringList("commands");
            int amount = 0;

            for(String s : customCommands) {
                if(amount >= maxCommands) break;

                String cmd;

                if(s.contains(": ")) {
                    String[] split = s.split(": ");

                    if(!_numberUtils.isStringInteger(split[0])) {
                        Debug.DropTable_InvalidChance.out(split[0]);
                        continue;
                    }

                    int chance = Integer.valueOf(split[0]);
                    int randomNumber = _random.nextInt(100);

                    if(randomNumber > chance) continue;

                    cmd = split[1];
                } else {
                    cmd = s;
                }

                cmd = cmd.replace("%player%", player.getName());
                cmd = cmd.replace("%Player%", player.getName());

                commandList.add(cmd);
                amount++;
            }

            return commandList;
        }
    }

    /**
     * This class is the SkillUtils but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class SkillUtils implements ISkillUtils {

        private String getMessage(String bossType, String skill) {
            String defaultMessage = _plugin.getBossesYML().getString("Bosses." + bossType + ".Boss.Skills.message");

            return _plugin.getSkillsYML().getString("Skills." + skill + ".customMessage", defaultMessage);
        }

        @Override
        public boolean isSkillRunnable(double chance) {
            int randomNumber = _random.nextInt(100) + 1;

            return randomNumber <= chance;
        }

        @Override
        public void onSkillMessage(List<LivingEntity> affectedEntities, String bossType, LivingEntity boss, String skill) {
            String message = getMessage(bossType, skill);
            String name = getBossUtils().getBossName(boss.getUniqueId());

            if(message == null || message.equals("")) return;
            if(message.contains("{boss}")) message = message.replace("{boss}", _messageUtils.translateString(name));
            if(message.contains("{skill}")) message = message.replace("{skill}", skill);

            for(LivingEntity livingEntity : affectedEntities) {
                if(livingEntity instanceof Player) {
                    Player player = (Player) livingEntity;

                    player.sendMessage(_messageUtils.translateString(message));
                }
            }
        }

    }

    /**
     * This class is the CommandUtils but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class CommandUtils implements ICommandUtils {

        @Override
        public Player strAsPlayer(String s, CommandSender commandSender) {
            if(Bukkit.getPlayer(s) == null) {
                Messages.NOT_ONLINE.msg(commandSender);
                return null;
            }

            return Bukkit.getPlayer(s);
        }

        @Override
        public Integer strAsInteger(String s, CommandSender commandSender) {
            if(_numberUtils.isStringInteger(s)) return Integer.valueOf(s);

            Messages.INVALID_INTEGER.msg(commandSender);
            return null;
        }

        @Override
        public Double strAsDouble(String s, CommandSender commandSender) {
            if(_numberUtils.isStringDouble(s)) return Double.valueOf(s);

            Messages.INVALID_DOUBLE.msg(commandSender);
            return null;
        }

        @Override
        public boolean isIntegerWithinItemStackSize(int i, CommandSender commandSender) {
            if(i > 64) {
                Messages.ITEMSTACK_MAX.msg(commandSender);
                return false;
            }

            if(i < 1) {
                Messages.ITEMSTACK_MIN.msg(commandSender);
                return false;
            }

            return true;
        }

        @Override
        public boolean isCommandSenderInstanceOfPlayer(CommandSender commandSender) {
            if(!(commandSender instanceof Player)) {
                Messages.MUST_BE_PLAYER.msg(commandSender);
                return false;
            }

            return true;
        }

    }

    /**
     * This class is the CommandCore but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class CommandCore implements ICommandCore {

        private JavaPlugin _plugin;
        private ICommand _bossCmd, _giveeggCmd;

        public CommandCore(JavaPlugin plugin) {
            _plugin = plugin;
            _bossCmd = new BossCmd(getCommandUtils(), getBossUtils(), getAutoSpawnUtils(), _numberUtils);
            _giveeggCmd = new GiveEggCmd(getCommandUtils(), _itemStackUtils);
        }

        @Override
        public void registerCommands() {
            _plugin.getCommand("boss").setExecutor(_bossCmd);
            _plugin.getCommand("giveegg").setExecutor(_giveeggCmd);
        }
    }

    /**
     * This class is the CommandCore but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class ListenerCore implements IListenerCore {

        private JavaPlugin _plugin;
        private boolean breakInCage, hookStackMob;

        public ListenerCore(JavaPlugin plugin) {
            _plugin = plugin;

            breakInCage = plugin.getConfig().getBoolean("Settings.Skills.disableBlockBreakInCage", true);
            hookStackMob = plugin.getConfig().getBoolean("Settings.HookIntoStackMob", false);
        }

        @Override
        public void registerListeners() {
            PluginManager pluginManager = _plugin.getServer().getPluginManager();

            pluginManager.registerEvents(new PlayerMoveListener(getBossUtils(), _actionBarReflection, _messageUtils), _plugin);
            pluginManager.registerEvents(new BossSpawnListener(getLocationManager(), _messageUtils, getBossUtils(), _itemStackUtils), _plugin);
            pluginManager.registerEvents(new BossDeathListener(getBossUtils(), getDropTableUtils(), getAutoSpawnUtils()), _plugin);
            pluginManager.registerEvents(new BossDamageListener(getBossUtils()), _plugin);
            pluginManager.registerEvents(new BossTrailListener(getBossUtils()), _plugin);
            pluginManager.registerEvents(new MinionDeathListener(getBossUtils()), _plugin);
            pluginManager.registerEvents(new BossWitherListener(getBossUtils()), _plugin);
            pluginManager.registerEvents(new ChunkUnloadListener(getBossUtils(), getAutoSpawnUtils()), _plugin);
            pluginManager.registerEvents(new BossSkillListener(getSkillUtils(), getBossUtils()), _plugin);
            pluginManager.registerEvents(new GUIListener(_messageUtils, _itemStackUtils), _plugin);
            pluginManager.registerEvents(new BossSpawnCommandListener(), _plugin);
            pluginManager.registerEvents(new BossDeathCommandListener(), _plugin);
            pluginManager.registerEvents(new ReTargetListener(_bossUtils), _plugin);

            if(breakInCage) pluginManager.registerEvents(new BossCageListener(), _plugin);
            if(pluginManager.getPlugin("StackMob") != null && hookStackMob) pluginManager.registerEvents(new BossStackListener(getBossUtils()), _plugin);
        }
    }

    /**
     * This class is the LocationManager but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class LocationManager implements ILocationManager {

        private boolean _useRegions, _useFactions, _useBlockedRegions, _useWorlds, useASkyBlock, onOwnIslandOnly;
        private List<String> _regionList, _blockedRegionList, _blockedWorlds;
        private IFactions _factions;
        private IWorldGuard _worldGuard;
        private aSkyblock aSkyBlock;

        public LocationManager() {
            reloadSettings();
        }

        @Override
        public void reloadSettings() {
            _useRegions = _plugin.getConfigYML().getBoolean("Settings.SpawnRegions.useRegions");
            _useFactions = _plugin.getConfigYML().getBoolean("Settings.SpawnRegions.useWarzone");
            _useBlockedRegions = _plugin.getConfigYML().getBoolean("Settings.BlockedRegions.enabled");
            _useWorlds = _plugin.getConfigYML().getBoolean("Settings.BlockedWorlds.enabled", false);
            _regionList = _plugin.getConfigYML().getStringList("Settings.SpawnRegions.spawnRegions");
            _blockedRegionList = _plugin.getConfigYML().getStringList("Settings.BlockedRegions.blockedRegions");
            _blockedWorlds = _plugin.getConfigYML().getStringList("Settings.BlockedWorlds.blockedWorlds");
            this.useASkyBlock = _plugin.getConfigYML().getBoolean("Settings.SpawnRegions.ASkyBlock.enabled", false);
            this.onOwnIslandOnly = _plugin.getConfigYML().getBoolean("Settings.SpawnRegions.ASkyBlock.onOwnIslandOnly", false);

            setupFactions();
            setupWorldGuard();
            setupASkyblock();
        }

        @Override
        public void setupFactions() {
            if(!_useFactions) return;

            if(Bukkit.getServer().getPluginManager().getPlugin("LegacyFactions") != null) {
                _factions = new LegacyFactions();
                return;
            }

            if(Bukkit.getServer().getPluginManager().getPlugin("Factions") == null) return;

            Plugin factions = Bukkit.getServer().getPluginManager().getPlugin("Factions");
            String version = factions.getDescription().getVersion();
            String uuidVer = "1.6.";
            String oneVer = "1.8.";

            if(version.startsWith(uuidVer)) {
                _factions = new FactionsUUID();
            } else if(version.startsWith(oneVer)) {
                _factions = new FactionsOne();
            } else {
                _factions = new MFactions();
            }
        }

        @Override
        public void setupWorldGuard() {
            if(Bukkit.getServer().getPluginManager().getPlugin("WorldGuard") == null) return;

            _worldGuard = new WorldGuard();
        }

        @Override
        public void setupASkyblock() {
            if(Bukkit.getServer().getPluginManager().getPlugin("ASkyBlock") == null) return;

            this.aSkyBlock = new aSkyblock();
        }

        @Override
        public boolean canSpawnBoss(Player player, Location location) {
            for(int x = location.getBlockX() - 2; x <= location.getBlockX() + 2; x++) {
                for(int y = location.getBlockY(); y <= location.getBlockY() + 2; y++) {
                    for(int z = location.getBlockZ() - 2; z <= location.getBlockZ() + 2; z++) {
                        Location l = new Location(location.getWorld(), x, y, z);
                        Block block = l.getBlock();

                        if((!(block.getType().equals(Material.AIR))) || (!(block.getType() != null))) {
                            if(PassthroughableBlocks.isPassthroughable(block)) continue;

                            return false;
                        }
                    }
                }
            }

            if(_useWorlds) {
                if(_blockedWorlds.contains(location.getWorld().getName())) return false;
            }

            if(_useBlockedRegions) {
                List<String> currentRegions = _worldGuard.getRegionNames(location);
                boolean b = false;

                for(String s : _blockedRegionList) {
                    if(currentRegions.contains(s)) {
                        b = true;
                        break;
                    }
                }

                if(b) return false;
            }

            if(_useFactions && _factions != null) {
                if(!_factions.isInWarzone(location)) return false;
            }

            if(_useRegions && _worldGuard != null) {
                List<String> currentRegions = _worldGuard.getRegionNames(location);
                boolean b = false;

                for(String s : _regionList) {
                    if(currentRegions.contains(s)) {
                        b = true;
                        break;
                    }
                }

                if(!b) return false;
            }

            if(this.useASkyBlock && this.aSkyBlock != null) {
                if(this.onOwnIslandOnly) {
                    return this.aSkyBlock.isOnOwnIsland(player);
                }
            }

            return true;
        }
    }

    /**
     * This class is the InventoryManager but it's hidden inside CompositeRoot so
     * it's harder to find if someone decompiles it.
     *
     */
    private class InventoryManager implements IInventoryManager {

        private final CustomBosses _plugin = CustomBosses.getInstance();
        private Map<Integer, IInventoryUtils> _mapOfPages = new HashMap<Integer, IInventoryUtils>();
        private Map<Integer, Map<Integer, String>> _mapOfEggs = new HashMap<Integer, Map<Integer, String>>();

        public InventoryManager() {
            fillMap();
        }

        @Override
        public void setMapOfPages(int i, IInventoryUtils inventoryUtils) {
            _mapOfPages.put(i, inventoryUtils);
        }

        @Override
        public Map<Integer, IInventoryUtils> getMapOfPages() {
            return _mapOfPages;
        }

        @Override
        public void fillMap() {
            _mapOfPages = new HashMap<Integer, IInventoryUtils>();
            int currentPage = 1;
            ConfigurationSection configurationSection = _plugin.getConfigYML().getConfigurationSection("BossGUI");
            IInventoryUtils inventoryUtils = new InventoryUtils(configurationSection, _itemStackUtils);

            inventoryUtils.createInventory();

            Inventory inventory = inventoryUtils.getInventory();
            ConfigurationSection cs = _plugin.getBossesYML().getConfigurationSection("Bosses");
            int slot = 0;

            for(String s : cs.getKeys(false)) {
                ItemStack is = _itemStackUtils.createItemStack(cs.getConfigurationSection(s + ".Item"), 1, null);

                if(inventory.firstEmpty() == -1) {

                    setMapOfPages(currentPage, inventoryUtils);

                    currentPage += 1;
                    inventoryUtils = new InventoryUtils(configurationSection, _itemStackUtils);
                    inventoryUtils.createInventory();
                    inventory = inventoryUtils.getInventory();
                    slot = inventory.firstEmpty();
                    inventory.setItem(slot, is);
                } else {
                    inventory.addItem(is);
                }

                setMapOfEggs(currentPage, slot, s);
                slot = inventory.firstEmpty();
            }

            setMapOfPages(currentPage, inventoryUtils);
        }

        @Override
        public void setMapOfEggs(int page, int i, String boss) {
            Map<Integer, String> map = new HashMap<Integer, String>();

            if(_mapOfEggs.containsKey(page)) {
                map.putAll(_mapOfEggs.get(page));
            }

            map.put(i, boss);
            _mapOfEggs.put(page, map);
        }

        @Override
        public Map<Integer, Map<Integer, String>> getMapOfEggs() {
            return _mapOfEggs;
        }
    }

}
