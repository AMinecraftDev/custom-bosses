package net.aminecraftdev.custombosses.commands;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.ICommandUtils;
import net.aminecraftdev.custombosses.utils.Messages;
import net.aminecraftdev.custombosses.utils.Permission;
import net.aminecraftdev.custombosses.zcore.i.ICommand;
import net.aminecraftdev.custombosses.zcore.i.IItemStackUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Charles on 13/3/2017.
 */
public class GiveEggCmd implements ICommand {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private ICommandUtils _commandUtils;
    private IItemStackUtils _itemStackUtils;

    public GiveEggCmd(ICommandUtils commandUtils, IItemStackUtils itemStackUtils) {
        _commandUtils = commandUtils;
        _itemStackUtils = itemStackUtils;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!Permission.bosses_admin.hasPermissionM(sender)) return false;

        if(args.length == 0) {
            Messages.COMMAND_GIVEEGG_INVALIDARGS.msg(sender);
            return false;
        }

        if(args.length == 2 || args.length == 3) {
            if(_commandUtils.strAsPlayer(args[0], sender) == null) return false;
            String bossType = args[1];
            int amount = 1;
            Player player = _commandUtils.strAsPlayer(args[0], sender);

            if(args.length == 3) {
                if(_commandUtils.strAsInteger(args[2], sender) == null) return false;

                amount = _commandUtils.strAsInteger(args[2], sender);

                if(!_commandUtils.isIntegerWithinItemStackSize(amount, sender)) return false;
            }

            if(!_plugin.getBossesYML().contains("Bosses." + bossType)) {
                Messages.COMMAND_GIVEEGG_INVALIDTYPE.msg(sender);
                return false;
            }

            String path = "Bosses." + bossType + ".Item";
            ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection(path);
            String name = configurationSection.getString("name");
            ItemStack itemStack = _itemStackUtils.createItemStack(configurationSection, amount, null);

            player.getInventory().addItem(itemStack);

            Messages.COMMAND_GIVEEGG_SENDER.msg(sender, amount, player);
            Messages.COMMAND_GIVEEGG_RECEIVER.msg(player, amount, name);
            return true;
        }

        Messages.COMMAND_GIVEEGG_INVALIDARGS.msg(sender);
        return false;
    }
}
