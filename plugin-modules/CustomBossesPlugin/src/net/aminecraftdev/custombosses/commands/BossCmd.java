package net.aminecraftdev.custombosses.commands;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.events.BossSpawnEvent;
import net.aminecraftdev.custombosses.utils.*;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.zcore.i.ICommand;
import net.aminecraftdev.custombosses.zcore.i.INumberUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * Created by Charles on 8/3/2017.
 */
public class BossCmd implements ICommand {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private ICommandUtils _commandUtils;
    private IBossUtils _bossUtils;
    private boolean _bossTimeEnabled;
    private String _sectionToTime;
    private IAutoSpawnUtils _autoSpawnUtils;
    private INumberUtils _numberUtils;

    public BossCmd(ICommandUtils commandUtils, IBossUtils bossUtils, IAutoSpawnUtils autoSpawnUtils, INumberUtils numberUtils) {
        _commandUtils = commandUtils;
        _bossUtils = bossUtils;
        _autoSpawnUtils = autoSpawnUtils;
        _numberUtils = numberUtils;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        boolean b = _plugin.getAutoSpawnsYML().getBoolean("Settings.enabled");

        if(args.length == 0) {
            Messages.COMMAND_BOSS_INVALIDARGS.msg(sender);
            return false;
        }

        /*

        /BOSS HELP

         */
        if(args[0].equalsIgnoreCase("help") && args.length == 1) {
            if(!Permission.bosses_command_help.hasPermissionM(sender)) return false;

            Messages.COMMAND_BOSS_HELP.msg(sender);
            return true;
        }

        /*

        /BOSS RELOAD

         */
        if(args[0].equalsIgnoreCase("reload") && args.length == 1) {
            if(!Permission.bosses_command_reload.hasPermissionM(sender)) return false;

            _plugin.reloadFiles();
            _plugin.refreshAllFields();
            Messages.COMMAND_BOSS_RELOAD.msg(sender);
            return true;
        }

        /*

        /BOSS KILLALL

         */
        if(args[0].equalsIgnoreCase("killall")) {
            if(!Permission.bosses_command_killall.hasPermissionM(sender)) return false;

            if(!_plugin.getConfigYML().getBoolean("Settings.KillAll.onCommand")) {
                Messages.COMMAND_BOSS_KILLALLBLOCKED.msg(sender);
                return false;
            }

            if(args.length == 1) {
                int minions = _bossUtils.removeAllMinions();
                int bosses = _bossUtils.removeAllBosses();
                int total = minions + bosses;

                Messages.COMMAND_BOSS_KILLALL.msg(sender, total);
                return true;
            } else if(args.length == 2) {
                String worldArg = args[1];
                World world = Bukkit.getWorld(worldArg);

                if(world == null) {
                    Messages.COMMAND_BOSS_KILLALLWORLD.msg(sender);
                    return false;
                }

                int minions = _bossUtils.removeAllMinions(world);
                int bosses = _bossUtils.removeAllBosses(world);
                int total = minions + bosses;

                Messages.COMMAND_BOSS_KILLALL.msg(sender, total);
                return true;
            }
        }

        /*

        /BOSS SPAWN (type) (player)

         */
        if(args[0].equalsIgnoreCase("spawn") && args.length == 3) {
            if(!Permission.bosses_command_spawn.hasPermissionM(sender)) return false;

            String bossName = args[1];

            if(!_plugin.getBossesYML().contains("Bosses." + bossName)) {
                Messages.COMMAND_BOSS_SPAWN_INVALIDBOSS.msg(sender);
                return false;
            }

            if(_commandUtils.strAsPlayer(args[2], sender) == null) return false;

            Player player = _commandUtils.strAsPlayer(args[2], sender);
            Location location = player.getLocation();

            if(location == null) {
                Messages.COMMAND_BOSS_SPAWN_INVALIDLOCATION.msg(sender);
                return false;
            }

            ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses." + bossName);
            LivingEntity boss = _bossUtils.spawnBoss(location, bossName, configurationSection);
            boolean broadcast = _plugin.getConfigYML().getBoolean("Settings.broadcastOnSpawnCommand", true);

            if(boss == null) return false;
            if(broadcast) Messages.COMMAND_BOSS_SPAWN_SPAWNED.broadcast(location, bossName);
            Bukkit.getPluginManager().callEvent(new BossSpawnEvent(sender, location, boss));
            return true;
        }

        /*

        /BOSS SPAWN (type) (x) (y) (z) (world)

         */
        if(args[0].equalsIgnoreCase("spawn") && args.length == 6) {
            if(!Permission.bosses_command_spawn.hasPermissionM(sender)) return false;

            String bossName = args[1];
            String sX = args[2];
            String sY = args[3];
            String sZ = args[4];
            String sWorld = args[5];

            if(!_plugin.getBossesYML().contains("Bosses." + bossName)) {
                Messages.COMMAND_BOSS_SPAWN_INVALIDBOSS.msg(sender);
                return false;
            }

            World world = Bukkit.getWorld(sWorld);

            if(world == null) {
                Messages.COMMAND_BOSS_SPAWN_INVALIDWORLD.msg(sender);
                return false;
            }

            if(_commandUtils.strAsInteger(sX, sender) == null) return false;
            if(_commandUtils.strAsInteger(sY, sender) == null) return false;
            if(_commandUtils.strAsInteger(sZ, sender) == null) return false;

            int x = Integer.valueOf(sX);
            int y = Integer.valueOf(sY);
            int z = Integer.valueOf(sZ);
            Location location = new Location(world, x, y, z);

            if(location == null) {
                Messages.COMMAND_BOSS_SPAWN_INVALIDLOCATION.msg(sender);
                return false;
            }

            ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses." + bossName);
            LivingEntity boss = _bossUtils.spawnBoss(location, bossName, configurationSection);

            if(boss == null) return false;

            Messages.COMMAND_BOSS_SPAWN_SPAWNED.broadcast(location, bossName);
            Bukkit.getPluginManager().callEvent(new BossSpawnEvent(sender, location, boss));

            return true;
        }

        /*

        /BOSS LIST

         */
        if(args[0].equalsIgnoreCase("list") && args.length == 1) {
            if(!Permission.bosses_command_list.hasPermissionM(sender)) return false;
            if(!_commandUtils.isCommandSenderInstanceOfPlayer(sender)) return false;
            Player player = (Player) sender;

            player.openInventory(_plugin.getInventoryManager().getMapOfPages().get(1).getInventory());
            Messages.COMMAND_BOSS_LIST.msg(player);
            return true;
        }

        /*

        /BOSS TIME

         */
        if(args[0].equalsIgnoreCase("time") && args.length == 1) {
            if(!Permission.bosses_command_time.hasPermissionM(sender)) return false;

            _bossTimeEnabled = _plugin.getAutoSpawnsYML().getBoolean("Settings.onBossTimeCommand.enabled");
            _sectionToTime = _plugin.getAutoSpawnsYML().getString("Settings.onBossTimeCommand.whichSectionToTime");

            if(!_bossTimeEnabled || !b) {
                Messages.COMMAND_BOSS_TIME_NOTENABLED.msg(sender);
                return false;
            }

            if(_sectionToTime == null) {
                Debug.AutoSpawn_NullKey.out();
                sender.sendMessage("---=[ FAILED TO LOAD (CHECK CONSOLE) ]=---");
                return false;
            }

            if(!_autoSpawnUtils.getMapOfAutoSpawns().containsKey(_sectionToTime)) {
                Debug.AutoSpawn_MapDoesntContainKey.out(_sectionToTime);
                sender.sendMessage("---=[ FAILED TO LOAD (CHECK CONSOLE) ]=---");
                return false;
            }

            int time = _autoSpawnUtils.getMapOfAutoSpawns().get(_sectionToTime).getTimer();
            String formatted = _numberUtils.formatTime(time);

            Messages.COMMAND_BOSS_TIME_TIME.msg(sender, formatted);
            return true;
        }

        Messages.COMMAND_BOSS_INVALIDARGS.msg(sender);
        return false;
    }
}
