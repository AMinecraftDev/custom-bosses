package net.aminecraftdev.custombosses.utils.placeholders;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.Placeholder;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.zcore.i.INumberUtils;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 11-Oct-17
 */
public class MVdWPlaceholders extends Placeholder {

    public MVdWPlaceholders(ConfigurationSection configurationSection, String key, IAutoSpawnUtils autoSpawnUtils, INumberUtils numberUtils) {
        super(configurationSection, key, autoSpawnUtils, numberUtils);

        PlaceholderAPI.registerPlaceholder(CustomBosses.getInstance(), _placeholder, event -> {
            if(!_autoSpawnEnabled) return "";

            if(_key == null) {
                Debug.AutoSpawn_NullKey.out();
                return "ERROR";
            }

            if(!_autoSpawnUtils.getMapOfAutoSpawns().containsKey(_key)) {
                Debug.AutoSpawn_MapDoesntContainKey.out(_key);
                return "ERROR";
            }

            int currentTime = _autoSpawnUtils.getMapOfAutoSpawns().get(_key).getTimer();

            return _numberUtils.formatTime(currentTime);
        });
    }
}
