package net.aminecraftdev.custombosses.utils.placeholders;

import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.gmail.filoghost.holographicdisplays.api.placeholder.PlaceholderReplacer;
import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.Placeholder;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.zcore.i.INumberUtils;
import org.bukkit.configuration.ConfigurationSection;

/**
 * Created by charl on 4/1/2017.
 */
public class HologramsPlaceholder extends Placeholder implements PlaceholderReplacer {

    public HologramsPlaceholder(ConfigurationSection configurationSection, String key, IAutoSpawnUtils autoSpawnUtils, INumberUtils numberUtils) {
        super(configurationSection, key, autoSpawnUtils, numberUtils);

        HologramsAPI.registerPlaceholder(CustomBosses.getInstance(), _placeholder, 1, this);
    }

    @Override
    public String update() {
        if(!_autoSpawnEnabled) return "";

        if(_key == null) {
            Debug.AutoSpawn_NullKey.out();
            HologramsAPI.unregisterPlaceholder(CustomBosses.getInstance(), _placeholder);
            return "ERROR";
        }

        if(!_autoSpawnUtils.getMapOfAutoSpawns().containsKey(_key)) {
            HologramsAPI.unregisterPlaceholder(CustomBosses.getInstance(), _placeholder);
            Debug.AutoSpawn_MapDoesntContainKey.out(_key);
            return "ERROR";
        }

        int currentTime = _autoSpawnUtils.getMapOfAutoSpawns().get(_key).getTimer();

        return _numberUtils.formatTime(currentTime);
    }
}
