package net.aminecraftdev.custombosses.utils;

import org.bukkit.Location;
import org.bukkit.block.BlockState;

import java.util.Queue;

/**
 * Created by charl on 18-Apr-17.
 */
public interface ICageBase {

    Queue<BlockState> getCageFlats(Location location);

    Queue<BlockState> getCageWalls(Location location);

    Queue<BlockState> getCageInside(Location location);
}
