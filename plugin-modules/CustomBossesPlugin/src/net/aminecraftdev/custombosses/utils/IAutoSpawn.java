package net.aminecraftdev.custombosses.utils;

import org.bukkit.scheduler.BukkitTask;

/**
 * Created by charl on 4/2/2017.
 */
public interface IAutoSpawn {

    void loadAllStats();

    void start();

    String spawnBoss();

    int getTimer();

    BukkitTask getTimerTask();
}
