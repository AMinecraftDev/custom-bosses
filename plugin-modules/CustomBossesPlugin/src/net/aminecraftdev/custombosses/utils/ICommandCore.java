package net.aminecraftdev.custombosses.utils;

/**
 * Created by Charles on 13/3/2017.
 */
public interface ICommandCore {

    void registerCommands();

}
