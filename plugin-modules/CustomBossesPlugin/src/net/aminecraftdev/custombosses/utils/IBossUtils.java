package net.aminecraftdev.custombosses.utils;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Charles on 8/3/2017.
 */
public interface IBossUtils {

    void reloadSettings();

    String getBossType(UUID livingEntity);

    String getBossName(UUID livingEntity);

    List<String> getMapOfDamages(UUID livingEntity);

    boolean isLivingEntityBoss(LivingEntity livingEntity);

    boolean isLivingEntityMinion(LivingEntity livingEntity);

    Object[] getTopPlayerArray(List<String> totalDamages);

    String getLocationString(Location location);

    Map<Integer, Player> getMapOfBossKillers(UUID livingEntity);

    double getPlayerDamage(String player, UUID livingEntity);

    List<String> getSpawnMessages(List<String> messages, String location, String type);

    List<String> getDeathMessage(List<String> totalDamages, UUID uuid, LivingEntity livingEntity);

    void onSpawnBroadcastMessage(Location location, String type, World world);

    void onDeathBroadcastMessage(Location location, List<String> totalDamages, UUID uuid, World world, LivingEntity livingEntity);

    LivingEntity spawnLivingEntity(Location location, ConfigurationSection configurationSection);

    LivingEntity spawnBoss(Location location, String type, ConfigurationSection configurationSection);

    LivingEntity spawnAutoBoss(Location location, String type, ConfigurationSection configurationSection, String timedSpawnSection);

    LivingEntity spawnMinion(Location location, ConfigurationSection configurationSection, LivingEntity target, UUID boss);

    Map<Enchantment, Integer> getMapOfEnchants(ConfigurationSection configurationSection, String type);

    ItemStack getArmorItemStack(String type, Map<Enchantment, Integer> enchants);

    void applyEquipmentToBoss(LivingEntity boss, String type, ConfigurationSection configurationSection);

    void removeBoss(LivingEntity livingEntity);

    void removeAutoBoss(LivingEntity livingEntity);

    int getMinionCount(LivingEntity livingEntity);

    void removeMinion(LivingEntity livingEntity);

    int removeAllMinions();

    int removeAllMinions(World world);

    int removeAllBosses();

    int removeAllBosses(World world);

    void addDamageToEntity(UUID livingEntity, Player damager, double damage);

    LivingEntity getClosestBoss(Location location);
}
