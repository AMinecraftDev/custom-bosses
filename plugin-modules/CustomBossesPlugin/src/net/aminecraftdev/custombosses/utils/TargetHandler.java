package net.aminecraftdev.custombosses.utils;

import net.aminecraftdev.custombosses.CustomBosses;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public abstract class TargetHandler {

    protected LivingEntity livingEntity;
    protected LivingEntity currentTarget;

    public TargetHandler(LivingEntity livingEntity) {
        this.livingEntity = livingEntity;
        this.currentTarget = null;
    }

    public boolean canTarget() {
        return (this.livingEntity instanceof Creature);
    }

    public Creature getCreatureInstance() {
        return (Creature) this.livingEntity;
    }

    public abstract void updateTarget();

    public void createAutoTarget() {
        new BukkitRunnable() {
            public void run() {
                updateTarget();

                if(livingEntity == null || livingEntity.isDead()) return;

                createAutoTarget();
                return;
            }
        }.runTaskLaterAsynchronously(CustomBosses.getInstance(), 100L);
    }

}
