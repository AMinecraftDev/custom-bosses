package net.aminecraftdev.custombosses.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.SimpleDateFormat;

/**
 * Created by Charles on 8/3/2017.
 */
public enum Messages {

    NO_PERMISSION("NoPermission", "&c&l** &cYou do not have permission to that! &c&l**"),
    MUST_BE_PLAYER("MustBePlayer", "&c&l** &cYou must be a player to use that! &c&l**"),
    INVALID_INTEGER("InvalidInteger", "&c&l** &cYou specified an invalid number! &c&l**"),
    INVALID_DOUBLE("InvalidDouble", "&c&l** &cYou specified an invalid double! &c&l**"),
    INVENTORY_SPACE("InventorySpace", "&c&l** &cYou do not have enough inventory space! &c&l**"),
    NOT_ONLINE("NotOnline", "&c&l** &cThe specified player is offline! &c&l**"),
    ITEMSTACK_MIN("&c&l** &cThe minimum ItemStack size is 1 &c&l**"),
    ITEMSTACK_MAX("&c&l** &cThe maximum ItemStack size is 64 &c&l**"),

    COMMAND_BOSS_INVALIDARGS("&c&l** &cIncorrect usage! &nUse /boss help for more help.&c&l **"),
    COMMAND_BOSS_HELP(
            " &4\n" +
            "&7&m-----------------&b&l[ &a&lCustomBosses&r &b&l]&7&m-----------------&r\n" +
            " &4\n" +
            "&6/boss spawn (type) (x) (y) (z) (world) &e&m-&f&o Spawn a boss at the coordinates.\n" +
            "&7/boss spawn (type) (player) &e&m-&f&o Spawn a boss on a player.\n" +
            "&6/boss time &e&m-&f&o View the time until the next boss auto-spawn.\n" +
            "&6/boss reload &e&m-&f&o Reloads the YML files for CustomBosses.\n" +
            "&6/boss killall &e&m-&f&o Kills all currently spawned bosses/minions.\n" +
            "&6/boss list &e&m-&f&o Opens a GUI with all the available boss eggs.\n" +
            "&6/giveegg (player) (boss) [amount] &e&m-&f&o Gives the specified player a boss egg.\n" +
            " &4\n" +
            "&7&m-----------------&b&l[ &a&lCustomBosses&r &b&l]&7&m-----------------&r\n" +
            " &4"),
    COMMAND_BOSS_RELOAD("&a&l[!] &fYou just reloaded the &3configs &ffor CustomBosses. &a&l[!]"),
    COMMAND_BOSS_KILLALLBLOCKED("&c&l** &cThe KillAll command is blocked! &c&l**"),
    COMMAND_BOSS_KILLALLWORLD("&c&l** &cThe world you have specified doesn't exist! **"),
    COMMAND_BOSS_KILLALL("&a&l[!] &fYou killed &e{i} &fbosses and minions! &a&l[!]"),
    COMMAND_BOSS_LIST("&a&l[!] &fNow viewing all custom boss eggs! &a&l[!]"),
    COMMAND_BOSS_SPAWN_INVALIDBOSS("&c&l[!] &4The specified boss is invalid! &c&l[!]"),
    COMMAND_BOSS_SPAWN_INVALIDWORLD("&c&l[!] &4The specified world is invalid! &c&l[!]"),
    COMMAND_BOSS_SPAWN_INVALIDLOCATION("&c&l[!] &cThe specified location is null! &c&l[!]"),
    COMMAND_BOSS_SPAWN_SPAWNED("&a&l[!] &fYou have spawned a &e{s} &fboss at &e{location}&f! &a&l[!]"),
    COMMAND_BOSS_TIME_NOTENABLED("&c&l[!] &cThis is not enabled! &c&l[!]"),
    COMMAND_BOSS_TIME_TIME("&a&l[!] &fThere is currently &e{s} &fleft until next auto boss spawn. &a&l[!]"),

    COMMAND_GIVEEGG_INVALIDARGS("&c&l[!] &cIncorrect usage! &nUse /giveegg (player) (boss) [amount]&c&l [!]"),
    COMMAND_GIVEEGG_INVALIDTYPE("&c&l[!] &cThe specified boss type is invalid! &c&l[!]"),
    COMMAND_GIVEEGG_SENDER("&a&l[!] &fYou have given &e{i}x &fboss egg(s) to &e{p}&f. &a&l[!]"),
    COMMAND_GIVEEGG_RECEIVER("&a&l[!] &fYou have received &e{i}x {s} &fboss egg(s)! &a&l[!]"),

    CANNOT_SPAWN("&c&l[!] &cYou cannot spawn a boss here! &c&l[!]"),
    DISARM("&c&l&n(!)&r &7YOU HAVE BEEN DISARMED! CHECK THE GROUND AROUND YOU FOR YOUR ITEM!"),
    PERCENTAGETOOLOW("&c&l[!] &cYour percentage was too low to receive a reward! &c&l[!]");


    private String path;
    private String msg;
    private static FileConfiguration LANG;

    Messages(String path, String start) {
        this.path = path;
        this.msg = start;
    }

    Messages(String string) {
        this.path = this.name().replace("_", ".");
        this.msg = string;
    }

    public static void setFile(FileConfiguration configuration) {
        LANG = configuration;
    }

    @Override
    public String toString() {
        String s = ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));

        return s;
    }

    public String toString(Object... args) {
        String s = ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, msg));

        return getMessageReplaced(s, args);
    }

    public String getDefault() {
        return this.msg;
    }

    public String getPath() {
        return this.path;
    }

    public void msg(CommandSender p, Object... args) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String s1 : split) {
                messageString(p, s1, args);
            }

        } else {
            messageString(p, s, args);
        }
    }

    public void broadcast(Object... args) {
        String s = toString();

        if(s.contains("\n")) {
            String[] split = s.split("\n");

            for(String s1 : split) {
                for(Player player : Bukkit.getOnlinePlayers()) {
                    messageString(player, s1, args);
                }
            }
        } else {
            for(Player player : Bukkit.getOnlinePlayers()) {
                messageString(player, s, args);
            }
        }
    }

    private void messageString(CommandSender player, String s, Object... args) {
        player.sendMessage(getMessageReplaced(s, args));
    }

    private String getMessageReplaced(String message, Object... args) {
        for(Object object : args) {
            if(object instanceof CommandSender) {
                message = message.replace("{p}", ((CommandSender) object).getName());
            }

            if(object instanceof Location) {
                Location location = (Location) object;
                String repl = location.getWorld().getName() + ", " + location.getBlockX() + ", " + location.getBlockY() + ", " + location.getBlockZ();

                message = message.replace("{location}", repl);
            }

            if(object instanceof String) {
                message = message.replace("{s}", ChatColor.translateAlternateColorCodes('&', ((String) object)));
            }

            if(object instanceof Integer) {
                message = message.replace("{i}", ""+((Integer) object));
            }

            if(object instanceof ItemStack) {
                message = message.replace("{is}", getItemStackName((ItemStack) object));
            }
        }

        return message;
    }

    private String getItemStackName(ItemStack itemStack) {
        String name = itemStack.getType().toString().replace("_", " ");

        if(itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {
            return itemStack.getItemMeta().getDisplayName();
        }

        return name;
    }

    public static void sendMessage(LivingEntity livingEntity, String message) {
        livingEntity.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

}
