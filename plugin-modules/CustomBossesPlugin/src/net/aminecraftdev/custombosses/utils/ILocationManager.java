package net.aminecraftdev.custombosses.utils;

import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Charles on 13/3/2017.
 */
public interface ILocationManager {

    void reloadSettings();

    void setupFactions();

    void setupWorldGuard();

    void setupASkyblock();

    boolean canSpawnBoss(Player player, Location location);

}
