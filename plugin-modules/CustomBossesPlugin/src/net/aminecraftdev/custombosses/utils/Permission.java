package net.aminecraftdev.custombosses.utils;

import org.bukkit.command.CommandSender;

/**
 * Created by Charles on 8/3/2017.
 */
public enum Permission {

    bosses_admin("bosses.command.*"),
    bosses_command_help("bosses.command.*"),
    bosses_command_reload("bosses.command.*"),
    bosses_command_killall("bosses.command.*"),
    bosses_command_spawn("bosses.command.*"),
    bosses_command_list("bosses.command.*"),
    bosses_command_time("bosses.command.*");

    private String _permission;
    private String[] _hierarchy;

    Permission() {
        _permission = name().replace("_", ".");
        _hierarchy = null;
    }

    Permission(String... hierarchy) {
        _permission = name().replace("_", ".");
        _hierarchy = hierarchy;
    }

    public String getPermission() {
        return _permission;
    }

    public boolean hasPermission(CommandSender commandSender) {
        if(_hierarchy != null) {
            for(String s : _hierarchy) {
                if(commandSender.hasPermission(s)) {
                    return true;
                }
            }
        }

        if(commandSender.hasPermission(getPermission())) return true;
        if(commandSender.hasPermission("bosses.*")) return true;
        return false;
    }

    public boolean hasPermissionM(CommandSender commandSender) {
        if(!hasPermission(commandSender)) {
            Messages.NO_PERMISSION.msg(commandSender);
            return false;
        }

        return true;
    }

}
