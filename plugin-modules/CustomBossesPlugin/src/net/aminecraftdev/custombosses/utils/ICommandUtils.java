package net.aminecraftdev.custombosses.utils;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Charles on 8/3/2017.
 */
public interface ICommandUtils {

    Player strAsPlayer(String s, CommandSender commandSender);

    Integer strAsInteger(String s, CommandSender commandSender);

    Double strAsDouble(String s, CommandSender commandSender);

    boolean isIntegerWithinItemStackSize(int i, CommandSender commandSender);

    boolean isCommandSenderInstanceOfPlayer(CommandSender commandSender);
}
