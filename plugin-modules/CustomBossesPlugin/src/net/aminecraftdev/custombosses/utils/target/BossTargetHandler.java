package net.aminecraftdev.custombosses.utils.target;

import net.aminecraftdev.custombosses.utils.TargetHandler;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class BossTargetHandler extends TargetHandler {

    public BossTargetHandler(LivingEntity livingEntity) {
        super(livingEntity);
    }

    @Override
    public void updateTarget() {
        if(!canTarget()) return;

        Creature creature = getCreatureInstance();
        double closestDistance = (50.0D * 50.0D);
        Player closestPlayer = null;

        for(Player player : creature.getWorld().getPlayers()) {
            if(player.getLocation().distanceSquared(creature.getLocation()) > closestDistance) continue;

            closestDistance = player.getLocation().distanceSquared(creature.getLocation());
            closestPlayer = player;
        }

        if(closestPlayer == null) return;

        creature.setTarget(closestPlayer);
        currentTarget = closestPlayer;
    }


}
