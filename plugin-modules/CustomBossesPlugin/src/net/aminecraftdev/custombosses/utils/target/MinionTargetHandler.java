package net.aminecraftdev.custombosses.utils.target;

import net.aminecraftdev.custombosses.utils.TargetHandler;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class MinionTargetHandler extends TargetHandler {

    private static final Random RANDOM = new Random();
    private LivingEntity bossLivingEntity;

    public MinionTargetHandler(LivingEntity livingEntity, LivingEntity bossLivingEntity) {
        super(livingEntity);

        this.bossLivingEntity = bossLivingEntity;
    }

    @Override
    public void updateTarget() {
        if(!canTarget()) return;

        Creature creature = getCreatureInstance();
        List<Player> nearbyPlayers = new ArrayList<Player>();

        if(this.bossLivingEntity == null) return;

        for(Entity entity : this.bossLivingEntity.getNearbyEntities(50, 10, 50)) {
            if(!(entity instanceof Player)) continue;

            nearbyPlayers.add((Player) entity);
        }

        if(nearbyPlayers.isEmpty()) return;

        int randomNumber = RANDOM.nextInt(nearbyPlayers.size());
        Player newTarget = nearbyPlayers.get(randomNumber);

        if((currentTarget != null) && (newTarget.getUniqueId().equals(currentTarget.getUniqueId()))) return;

        creature.setTarget(nearbyPlayers.get(randomNumber));
        currentTarget = nearbyPlayers.get(randomNumber);
    }
}
