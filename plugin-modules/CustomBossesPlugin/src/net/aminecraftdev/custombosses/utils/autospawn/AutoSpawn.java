package net.aminecraftdev.custombosses.utils.autospawn;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.events.BossSpawnEvent;
import net.aminecraftdev.custombosses.utils.IAutoSpawn;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.zcore.i.IChunkReflection;
import net.aminecraftdev.custombosses.zcore.i.IMessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.List;
import java.util.Random;

/**
 * Created by charl on 4/2/2017.
 */
public class AutoSpawn implements IAutoSpawn {

    private static final CustomBosses _plugin = CustomBosses.getInstance();
    private ConfigurationSection _configurationSection;
    private IAutoSpawnUtils _autoSpawnUtils;
    private IChunkReflection _chunkReflection;
    private IMessageUtils _messageUtils;
    private IBossUtils _bossUtils;
    private Random _random = new Random();
    private Location _spawnLocation;
    private int _maxSpawnedBosses, _timerTime, _spawnRate, _bossesPerInterval;
    private boolean _spawnIfChunkNotLoaded;
    private List<String> _spawnedMessage, _possibleSpawns;
    private String _spawnType, _sectionName;
    private BukkitTask timerTask;

    public AutoSpawn(ConfigurationSection configurationSection, IAutoSpawnUtils autoSpawnUtils, IChunkReflection chunkReflection, String sectionName, IBossUtils bossUtils, IMessageUtils messageUtils) {
        _configurationSection = configurationSection;
        _autoSpawnUtils = autoSpawnUtils;
        _chunkReflection = chunkReflection;
        _sectionName = sectionName;
        _bossUtils = bossUtils;
        _messageUtils = messageUtils;
    }

    @Override
    public void loadAllStats() {
        _spawnLocation = _autoSpawnUtils.getLocation(_configurationSection.getConfigurationSection("Coords"));
        _spawnedMessage = _configurationSection.getStringList("message");
        _maxSpawnedBosses = _configurationSection.getInt("Settings.maxSpawned");
        _bossesPerInterval = _configurationSection.getInt("Settings.bossesPerInterval");
        _spawnRate = _autoSpawnUtils.getSpawnRate(_configurationSection);
        _spawnIfChunkNotLoaded = _configurationSection.getBoolean("Settings.spawnIfChunkIsntLoaded");

        _spawnType = _configurationSection.getString("Settings.spawnType");
        _possibleSpawns = _configurationSection.getStringList("PossibleSpawns");
    }

    @Override
    public void start() {
        _timerTime = _autoSpawnUtils.getRandomSpawnTime(_spawnRate, _spawnType);
        timerTask = new BukkitRunnable() {

            public void run() {
                _timerTime -= 1;

                if(_timerTime <= 0) {
                    _spawnLocation = _autoSpawnUtils.getLocation(_configurationSection.getConfigurationSection("Coords"));
                    start();

                    boolean canSpawn = _autoSpawnUtils.canSpawn(_spawnLocation, _maxSpawnedBosses, _bossesPerInterval, _sectionName, _spawnIfChunkNotLoaded);

                    if(canSpawn) {
                        if(!_chunkReflection.isChunkLoaded(_spawnLocation)) {
                            _chunkReflection.loadChunk(_spawnLocation);
                        }

                        String location = _spawnLocation.getBlockX() + ", " + _spawnLocation.getBlockY() + ", " + _spawnLocation.getBlockZ();
                        String boss = "";

                        for(int i = 1; i <= _bossesPerInterval; i++) {
                            String s = spawnBoss();

                            if(boss.equals("")) continue;
                            boss = s;
                        }

                        for(String s : _spawnedMessage) {
                            if(s.contains("{loc}")) s = s.replace("{loc}", location);
                            if(s.contains("{boss}")) s = s.replace("{boss}", boss);

                            Bukkit.broadcastMessage(_messageUtils.translateString(s));
                        }
                    }

                    cancel();
                }
            }
        }.runTaskTimer(CustomBosses.getInstance(), 20L, 20L);
    }

    @Override
    public String spawnBoss() {
        int randomNumber = _random.nextInt(_possibleSpawns.size());
        String boss = _possibleSpawns.get(randomNumber);

        if(!_autoSpawnUtils.isValidBoss(boss)) {
            Debug.AutoSpawn_InvalidBoss.out(boss, _sectionName);
            return "";
        }

        ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses." + boss);

        LivingEntity livingEntity = _bossUtils.spawnAutoBoss(_spawnLocation, boss, configurationSection, _sectionName);
        Bukkit.getPluginManager().callEvent(new BossSpawnEvent(null, _spawnLocation, livingEntity, configurationSection));

        return configurationSection.getString("Boss.name");

    }

    @Override
    public int getTimer() {
        return _timerTime;
    }

    @Override
    public BukkitTask getTimerTask() {
        return timerTask;
    }

}
