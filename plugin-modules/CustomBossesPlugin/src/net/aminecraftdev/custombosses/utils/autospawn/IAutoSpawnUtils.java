package net.aminecraftdev.custombosses.utils.autospawn;

import net.aminecraftdev.custombosses.utils.IAutoSpawn;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;

import java.util.List;
import java.util.Map;

/**
 * Created by Charles on 8/3/2017.
 */
public interface IAutoSpawnUtils {
    void refreshAutoSpawns();

    void loadAutoSpawns();

    List<String> getListOfActivatedHologramPlaceholders();

    Map<String, IAutoSpawn> getMapOfAutoSpawns();

    List<LivingEntity> getListOfAutoBosses();

    Map<String, List<LivingEntity>> getMapOfAutoBosses();

    Map<String, Integer> getMapOfAutoSpawnAmounts();

    boolean isLivingEntityAutoBoss(LivingEntity livingEntity);

    Location getLocation(ConfigurationSection configurationSection);

    int getSpawnRate(ConfigurationSection configurationSection);

    int getRandomSpawnTime(int duration, String spawnType);

    boolean canSpawn(Location location, int maxAmount, int spawnAmount, String sectionName, boolean spawnIfChunkNotLoaded);

    boolean isValidBoss(String boss);
}
