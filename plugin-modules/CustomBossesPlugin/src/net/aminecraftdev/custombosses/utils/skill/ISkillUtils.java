package net.aminecraftdev.custombosses.utils.skill;

import org.bukkit.entity.LivingEntity;

import java.util.List;

/**
 * Created by Charles on 8/3/2017.
 */
public interface ISkillUtils {

    boolean isSkillRunnable(double chance);

    void onSkillMessage(List<LivingEntity> affectedEntities, String bossType, LivingEntity boss, String skill);
}
