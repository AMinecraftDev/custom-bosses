package net.aminecraftdev.custombosses.utils.skill;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Charles on 27/3/2017.
 */
public interface ISkillHandler {

    Player getDamager();

    String getSkill();

    ConfigurationSection getSkillConfigSection();

    Location getLocation();

    SkillTypes getSkillType();

    List<LivingEntity> getListOfTargettedPlayers();
}
