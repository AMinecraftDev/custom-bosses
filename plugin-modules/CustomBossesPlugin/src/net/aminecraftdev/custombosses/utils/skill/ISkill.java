package net.aminecraftdev.custombosses.utils.skill;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * Created by Charles on 27/3/2017.
 */
public interface ISkill {

    void handleSkill(LivingEntity player, Location location);

}
