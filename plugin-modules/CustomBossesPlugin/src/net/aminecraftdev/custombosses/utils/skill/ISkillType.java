package net.aminecraftdev.custombosses.utils.skill;

import org.bukkit.entity.LivingEntity;

import java.util.List;

/**
 * Created by Charles on 27/3/2017.
 */
public interface ISkillType {

    void runSkill(List<LivingEntity> targettedPlayers, String skill, LivingEntity boss);

}
