package net.aminecraftdev.custombosses.utils.skill;

import net.aminecraftdev.custombosses.skills.types.Command;
import net.aminecraftdev.custombosses.skills.types.Custom;
import net.aminecraftdev.custombosses.skills.types.Message;
import net.aminecraftdev.custombosses.skills.types.Potion;
import net.aminecraftdev.custombosses.zcore.c.PotionUtils;

/**
 * Created by Charles on 27/3/2017.
 */
public enum SkillTypes {

    POTION(new Potion(new PotionUtils())),
    COMMAND(new Command()),
    MESSAGE(new Message()),
    CUSTOM(new Custom());

    private ISkillType _skillType;
    private String _name;

    SkillTypes(ISkillType skillType) {
        _skillType = skillType;
        _name = name().toUpperCase();
    }

    public String getName() {
        return _name;
    }

    public ISkillType getSkillType() {
        return _skillType;
    }

    public static SkillTypes getSkillType(String name) {
        for(SkillTypes s : SkillTypes.values()) {
            if(name.equalsIgnoreCase(s.getName())) {
                return s;
            }
        }

        return null;
    }

}
