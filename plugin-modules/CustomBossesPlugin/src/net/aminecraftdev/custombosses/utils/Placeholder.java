package net.aminecraftdev.custombosses.utils;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.zcore.i.INumberUtils;
import org.bukkit.configuration.ConfigurationSection;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 11-Oct-17
 */
public class Placeholder {

    protected String _key, _placeholder;
    protected IAutoSpawnUtils _autoSpawnUtils;
    protected INumberUtils _numberUtils;
    protected boolean _autoSpawnEnabled;

    public Placeholder(ConfigurationSection configurationSection, String key, IAutoSpawnUtils autoSpawnUtils, INumberUtils numberUtils) {
        _placeholder = configurationSection.getString("Settings.holographicPlaceholder");
        _autoSpawnEnabled = CustomBosses.getInstance().getAutoSpawnsYML().getBoolean("Settings.enabled", false);

        _autoSpawnUtils = autoSpawnUtils;
        _key = key;
        _numberUtils = numberUtils;

        Debug.HolographicDisplay_PlaceholderLoaded.out(_placeholder, key);
    }

}
