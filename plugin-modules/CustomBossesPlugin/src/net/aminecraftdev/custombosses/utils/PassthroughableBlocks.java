package net.aminecraftdev.custombosses.utils;

import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charles on 13/3/2017.
 */
public enum PassthroughableBlocks {

    AIR(Material.AIR, 0),
    SAPLING(Material.SAPLING, 6),
    COBWEB(Material.WEB, 30),
    SHRUB(Material.DEAD_BUSH, 31),
    FLOWER1(Material.YELLOW_FLOWER, 37),
    FLOWER2(Material.RED_ROSE, 38),
    MUSHROOM1(Material.BROWN_MUSHROOM, 39),
    MUSHROOM2(Material.RED_MUSHROOM, 40),
    TORCH(Material.TORCH, 50),
    SNOW(Material.SNOW, 78),
    VINES(Material.VINE, 106),
    CARPET(Material.CARPET, 171),
    SUNFLOWER(Material.DOUBLE_PLANT, 175),
    PAINTING(Material.PAINTING, 321),
    SIGN(Material.SIGN, 323),
    ITEMFRAME(Material.ITEM_FRAME, 389),
    LEVER(Material.LEVER, 69),
    PRESSUREPLATE1(Material.STONE_PLATE, 70),
    PRESSUREPLATE2(Material.WOOD_PLATE, 72),
    REDSTONETORCH1(Material.REDSTONE_TORCH_ON, 76),
    REDSTONETORCH2(Material.REDSTONE_TORCH_OFF, 76),
    BUTTON1(Material.WOOD_BUTTON, 143),
    BUTTON2(Material.STONE_BUTTON, 77),
    TRIPWIREHOOK(Material.TRIPWIRE_HOOK, 131),
    REDSTONE(Material.REDSTONE, 331),
    RAIL1(Material.POWERED_RAIL, 27),
    RAIL2(Material.DETECTOR_RAIL, 28),
    RAIL3(Material.RAILS, 66),
    RAIL4(Material.ACTIVATOR_RAIL, 157),
    WATER1(Material.WATER, 8),
    WATER2(Material.STATIONARY_WATER, 9),
    LAVA1(Material.LAVA, 10),
    LAVA2(Material.STATIONARY_LAVA, 11);

    private Material material;
    private int id;
    private static List<Integer> BY_ID = new ArrayList<Integer>();
    private static List<Material> BY_MAT = new ArrayList<Material>();

    PassthroughableBlocks(Material material, int id) {
        this.id = id;
        this.material = material;
    }

    private int getId() {
        return this.id;
    }

    private Material getMaterial() {
        return this.material;
    }

    @SuppressWarnings("deprecation")
    public static boolean isPassthroughable(Block b) {
        Material type = b.getType();
        int id = b.getTypeId();

        if (BY_ID.contains(id) || BY_MAT.contains(type)) {
            return true;
        }

        return false;
    }

    static {
        for (PassthroughableBlocks b : PassthroughableBlocks.values()) {
            BY_ID.add(b.getId());
            BY_MAT.add(b.getMaterial());
        }
    }
}