package net.aminecraftdev.custombosses.utils;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;

/**
 * Created by Charles on 8/3/2017.
 */
public interface IDropTableUtils {

    void handleDropTable(String dropTable, UUID uuid);

    List<ItemStack> getRandomBossCustomDrops(ConfigurationSection configurationSection);

    List<ItemStack> getBossCustomDrops(ConfigurationSection configurationSection);

    List<String> getRandomBossCommands(ConfigurationSection configurationSection, Player player);

    List<String> getBossCommands(ConfigurationSection configurationSection, Player player);
}
