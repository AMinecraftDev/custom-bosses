package net.aminecraftdev.custombosses.utils;

import net.aminecraftdev.custombosses.zcore.i.IInventoryUtils;

import java.util.Map;

/**
 * Created by charl on 4/1/2017.
 */
public interface IInventoryManager {

    void setMapOfPages(int i, IInventoryUtils inventoryUtils);

    Map<Integer, IInventoryUtils> getMapOfPages();

    void fillMap();

    void setMapOfEggs(int page, int i, String boss);

    Map<Integer, Map<Integer, String>> getMapOfEggs();

}
