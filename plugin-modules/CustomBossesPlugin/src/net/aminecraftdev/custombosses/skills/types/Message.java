package net.aminecraftdev.custombosses.skills.types;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.Messages;
import net.aminecraftdev.custombosses.utils.skill.ISkillType;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import org.bukkit.entity.LivingEntity;

import java.util.List;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 23-Jun-18
 */
public class Message implements ISkillType {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void runSkill(List<LivingEntity> targettedPlayers, String skill, LivingEntity boss) {
        if(skill == null) {
            Debug.Skill_Null.out();
            return;
        }

        if(skill.equals("")) {
            Debug.Skill_Empty.out();
            return;
        }

        List<String> messages = _plugin.getSkillsYML().getStringList("Skills." + skill + ".messages");

        if(messages.isEmpty()) {
            Debug.Skill_Message_EmptyList.out(skill);
            return;
        }

        for(LivingEntity livingEntity : targettedPlayers) {
            for(String s : messages) {
                Messages.sendMessage(livingEntity, s);
            }
        }
    }

}