package net.aminecraftdev.custombosses.skills.types;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.utils.skill.ISkillType;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;

import java.util.List;

/**
 * Created by Charles on 27/3/2017.
 */
public class Command implements ISkillType {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void runSkill(List<LivingEntity> targettedPlayers, String skill, LivingEntity boss) {
        if(skill == null) {
            Debug.Skill_Null.out();
            return;
        }

        if(skill.equals("")) {
            Debug.Skill_Empty.out();
            return;
        }

        List<String> commands = _plugin.getSkillsYML().getStringList("Skills." + skill + ".commands");

        if(commands.isEmpty()) {
            Debug.Skill_Command_EmptyList.out(skill);
            return;
        }

        for(LivingEntity livingEntity : targettedPlayers) {
            for(String s : commands) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.replace("{p}", livingEntity.getName()));
            }
        }
    }

}
