package net.aminecraftdev.custombosses.skills.types;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import net.aminecraftdev.custombosses.utils.skill.ISkillType;
import org.bukkit.entity.LivingEntity;

import java.util.List;

/**
 * Created by Charles on 27/3/2017.
 */
public class Custom implements ISkillType {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void runSkill(List<LivingEntity> targettedPlayers, String skill, LivingEntity boss) {
        if(skill == null) {
            Debug.Skill_Null.out();
            return;
        }

        if(skill.equals("")) {
            Debug.Skill_Empty.out();
            return;
        }

        ISkill customSkill = null;
        String custom = _plugin.getSkillsYML().getString("Skills." + skill + ".customType");

        if(custom == null) {
            Debug.Skill_Custom_Null.out(skill);
            return;
        }

        if(custom.equals("")) {
            Debug.Skill_Custom_Empty.out(skill);
            return;
        }

        try {
            Class<?> clazz = Class.forName("net.aminecraftdev.custombosses.skills.base." + custom);

            if(ISkill.class.isAssignableFrom(clazz)) {
                customSkill = (ISkill) clazz.getConstructor().newInstance();
            }
        } catch (Exception e) {
            Debug.Skill_Custom_BaseNotFound.out(skill);
            return;
        }

        if(customSkill == null) {
            Debug.Skill_Custom_BaseNotFound.out(skill);
            return;
        }

        SkillBase.setLivingEntity(boss);
        SkillBase.setSkillType(skill);

        for(LivingEntity livingEntity : targettedPlayers) {
            customSkill.handleSkill(livingEntity, boss.getLocation());
        }
    }
}
