package net.aminecraftdev.custombosses.skills.types;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.skill.ISkillType;
import net.aminecraftdev.custombosses.zcore.i.IPotionUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;

import java.util.List;

/**
 * Created by Charles on 27/3/2017.
 */
public class Potion implements ISkillType {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private IPotionUtils _potionUtils;

    public Potion(IPotionUtils potionUtils) {
        _potionUtils = potionUtils;
    }

    @Override
    public void runSkill(List<LivingEntity> targettedPlayers, String skill, LivingEntity boss) {
        ConfigurationSection configurationSection = _plugin.getSkillsYML().getConfigurationSection("Skills." + skill + ".Potions");

        for(String s : configurationSection.getKeys(false)) {
            ConfigurationSection potionSection = configurationSection.getConfigurationSection(s);
            PotionEffect potionEffect = _potionUtils.getPotionEffect(potionSection);

            if(potionEffect == null) continue;

            for(LivingEntity livingEntity : targettedPlayers) {
                livingEntity.addPotionEffect(potionEffect);
            }
        }
    }
}
