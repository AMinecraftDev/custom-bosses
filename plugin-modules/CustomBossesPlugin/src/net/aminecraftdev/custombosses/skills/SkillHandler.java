package net.aminecraftdev.custombosses.skills;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.skill.ISkillHandler;
import net.aminecraftdev.custombosses.utils.skill.SkillTypes;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Charles on 27/3/2017.
 */
public class SkillHandler implements ISkillHandler {

    private static final CustomBosses PLUGIN = CustomBosses.getInstance();
    private String _skill, _path, _mode;
    private Player _damager;
    private LivingEntity _bossEntity;
    private Location _location;
    private SkillTypes _skillType;
    private int _radius;

    public SkillHandler(String skill, LivingEntity boss, Player damager) {
        _skill = skill;
        _path = "Skills." + _skill;
        _bossEntity = boss;
        _location = _bossEntity.getLocation();
        _damager = damager;

        if(!getSkillConfigSection().contains("type")) {
            Debug.Skill_Type_NotFound.out(getSkillConfigSection().getName());
            return;
        }

        if(SkillTypes.getSkillType(getSkillConfigSection().getString("type")) == null) {
            Debug.Skill_Type_DoesntExist.out(getSkillConfigSection().getShortList("type"), getSkillConfigSection().getName());
            return;
        }

        _skillType = SkillTypes.getSkillType(getSkillConfigSection().getString("type"));
        _mode = getSkillConfigSection().getString("mode");
        _radius = getSkillConfigSection().getInt("radius");
    }

    @Override
    public Player getDamager() {
        return _damager;
    }

    @Override
    public String getSkill() {
        return _skill;
    }

    @Override
    public ConfigurationSection getSkillConfigSection() {
        return PLUGIN.getSkillsYML().getConfigurationSection(_path);
    }

    @Override
    public Location getLocation() {
        return _location;
    }

    @Override
    public SkillTypes getSkillType() {
        return _skillType;
    }

    @Override
    public List<LivingEntity> getListOfTargettedPlayers() {
        List<LivingEntity> list = new ArrayList<LivingEntity>();
        int radiusSqr = _radius * _radius;
        Random r = new Random();

        if(_mode.equalsIgnoreCase("ONE")) {
            if(isTargetable(getDamager())) list.add(getDamager());
        } else if(_mode.equalsIgnoreCase("BOSS")) {
            list.add(_bossEntity);
        } else {
            for(Player player : Bukkit.getOnlinePlayers()) {
                if(player.getWorld() != _location.getWorld()) continue;
                if(getLocation().distanceSquared(player.getLocation()) > radiusSqr) continue;
                if(!isTargetable(player)) continue;

                if(_mode.equalsIgnoreCase("ALL")) {

                    list.add(player);

                } else if(_mode.equalsIgnoreCase("RANDOM")) {

                    int rNum = r.nextInt(100);

                    if(rNum >= 50) {
                        list.add(player);
                    }

                }
            }

        }

        return list;
    }

    private boolean isTargetable(Player player) {
        if(player.getGameMode() == GameMode.CREATIVE) return false;
        return true;
    }

}
