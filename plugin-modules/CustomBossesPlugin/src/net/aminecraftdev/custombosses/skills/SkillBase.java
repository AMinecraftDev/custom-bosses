package net.aminecraftdev.custombosses.skills;

import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.entity.LivingEntity;

import java.util.*;

/**
 * Created by Charles on 28/3/2017.
 */
public class SkillBase {

    private static Map<Location, BlockState> _mapOfOldBlock = new HashMap<Location, BlockState>();
    private static Map<Location, Integer> _mapOfCagesOnSingleLocation = new HashMap<Location, Integer>();
    private static Set<UUID> _setOfPlayersInCage = new HashSet<UUID>();
    private static Map<UUID, Map<String, Queue<BlockState>>> _mapOfCages = new HashMap<UUID, Map<String, Queue<BlockState>>>();
    private static Map<UUID, Map<String, Queue<BlockState>>> _backupMapOfCages = new HashMap<UUID, Map<String, Queue<BlockState>>>();
    private static Map<Location, Location> _mapOfCageLocations = new HashMap<Location, Location>();
    private static IBossUtils _bossUtils;
    private static LivingEntity _livingEntity;
    private static String _skillType;

    public static IBossUtils getBossUtils() {
        return _bossUtils;
    }

    public static void setBossUtils(IBossUtils bossUtils) {
        _bossUtils = bossUtils;
    }

    public static LivingEntity getLivingEntity() {
        return _livingEntity;
    }

    public static void setLivingEntity(LivingEntity livingEntity) {
        _livingEntity = livingEntity;
    }

    public static String getSkillType() {
        return _skillType;
    }

    public static void setSkillType(String skillType) {
        _skillType = skillType;
    }

    public static Map<Location, Location> getMapOfCageLocations() {
        return _mapOfCageLocations;
    }

    public static Map<UUID, Map<String, Queue<BlockState>>> getMapOfCages() {
        return _mapOfCages;
    }

    public static Map<UUID, Map<String, Queue<BlockState>>> getBackupMapOfCages() {
        return _backupMapOfCages;
    }

    public static Set<UUID> getSetOfPlayersInCage() {
        return _setOfPlayersInCage;
    }

    public static Map<Location, Integer> getMapOfCagesOnSingleLocation() {
        return _mapOfCagesOnSingleLocation;
    }

    public static Map<Location, BlockState> getMapOfOldBlock() {
        return _mapOfOldBlock;
    }

}
