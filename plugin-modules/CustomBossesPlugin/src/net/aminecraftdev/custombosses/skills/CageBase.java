package net.aminecraftdev.custombosses.skills;

import net.aminecraftdev.custombosses.utils.ICageBase;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by charl on 18-Apr-17.
 */
public class CageBase implements ICageBase {

    @Override
    public Queue<BlockState> getCageFlats(Location location) {
        World world = location.getWorld();
        Queue<BlockState> blockStateQueue = new LinkedList<BlockState>();
        Queue<Location> locationQueue = new LinkedList<Location>();

        for(int x = 1; x >= -1; x--) {
            for(int z = 1; z >= -1; z--) {
                Location location1 = new Location(world, x, +2, z);
                Location location2 = new Location(world, x, -1, z);

                locationQueue.add(location1);
                locationQueue.add(location2);
            }
        }

        locationQueue.add(new Location(world, +1, +2, -1));
        locationQueue.add(new Location(world, +1, +2, +0));

        while(!locationQueue.isEmpty()) {
            Location temp = locationQueue.poll();
            Block block = world.getBlockAt(temp.add(location).clone());

            blockStateQueue.add(block.getState());
        }

        return blockStateQueue;
    }

    @Override
    public Queue<BlockState> getCageWalls(Location location) {
        World world = location.getWorld();
        Queue<BlockState> blockStateQueue = new LinkedList<BlockState>();
        Queue<Location> locationQueue = new LinkedList<Location>();

        for(int x = 1; x >= -1; x--) {
            for(int z = 1; z >= -1; z--) {
                Location location1 = new Location(world, x, 1, z);
                Location location2 = new Location(world, x, 0, z);

                locationQueue.add(location1);
                locationQueue.add(location2);
            }
        }

        while(!locationQueue.isEmpty()) {
            Location temp = locationQueue.poll();
            Block block = world.getBlockAt(temp.add(location).clone());

            blockStateQueue.add(block.getState());
        }

        return blockStateQueue;
    }

    @Override
    public Queue<BlockState> getCageInside(Location location) {
        World world = location.getWorld();
        Queue<BlockState> blockStateQueue = new LinkedList<BlockState>();
        Queue<Location> locationQueue = new LinkedList<Location>();

        for(int y = 1; y >= 0; y--) {
            Location innerLocation = new Location(world, 0, y, 0);

            locationQueue.add(innerLocation);
        }

        while(!locationQueue.isEmpty()) {
            Location temp = locationQueue.poll();
            Block block = world.getBlockAt(temp.add(location).clone());

            blockStateQueue.add(block.getState());
        }

        return blockStateQueue;
    }

}
