package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

/**
 * Created by charl on 17-Apr-17.
 */
public class FIREBALL extends SkillBase implements ISkill {

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        final Vector v = player.getLocation().subtract(location).toVector().normalize().multiply(2);
        final Fireball largeFireball = (Fireball) location.getWorld().spawn(location, Fireball.class);

        largeFireball.setShooter(getLivingEntity());
        largeFireball.setDirection(v);
        largeFireball.setIsIncendiary(false);
    }

}
