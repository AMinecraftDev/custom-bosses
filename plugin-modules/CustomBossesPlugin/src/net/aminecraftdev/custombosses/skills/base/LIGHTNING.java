package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/**
 * Created by charl on 17-Apr-17.
 */
public class LIGHTNING implements ISkill {

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        player.getWorld().strikeLightningEffect(player.getEyeLocation());
    }
}
