package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/**
 * Created by Charles on 27/3/2017.
 */
public class WARP extends SkillBase implements ISkill {

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        if(getLivingEntity() == null) return;

        getLivingEntity().teleport(player);
    }
}
