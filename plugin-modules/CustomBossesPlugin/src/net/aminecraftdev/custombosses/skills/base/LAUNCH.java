package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class LAUNCH extends SkillBase implements ISkill {

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        player.teleport(getLivingEntity().getLocation().add(0, 35, 0));
    }
}
