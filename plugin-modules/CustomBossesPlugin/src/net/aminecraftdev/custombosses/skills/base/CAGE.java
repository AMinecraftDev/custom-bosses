package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.CageBase;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.ICageBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by charl on 18-Apr-17.
 */
public class CAGE extends SkillBase implements ISkill {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private Material _wallType, _flatType, _insideType;
    private ICageBase _cageBase = new CageBase();

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        final UUID playerUUID = player.getUniqueId();

        _wallType = Material.getMaterial(_plugin.getSkillsYML().getString("Skills." + getSkillType() + ".Cage.Wall").toUpperCase());
        _flatType = Material.getMaterial(_plugin.getSkillsYML().getString("Skills." + getSkillType() + ".Cage.Flat").toUpperCase());
        _insideType = Material.getMaterial(_plugin.getSkillsYML().getString("Skills." + getSkillType() + ".Cage.Inside").toUpperCase());

        if(getSetOfPlayersInCage().contains(playerUUID)) return;
        getSetOfPlayersInCage().add(playerUUID);

        final Location playerLocation = player.getLocation();
        final Location teleportLocation = new Location(player.getWorld(), playerLocation.getBlockX() + 0.5, playerLocation.getBlockY(), playerLocation.getBlockZ() + 0.5, playerLocation.getYaw(), playerLocation.getPitch());

        player.teleport(teleportLocation);

        final Location newPlayerLocation = player.getLocation();
        final Queue<BlockState> walls = _cageBase.getCageWalls(newPlayerLocation), flats = _cageBase.getCageFlats(newPlayerLocation), inside = _cageBase.getCageInside(newPlayerLocation);
        final Map<String, Queue<BlockState>> originalMap = new HashMap<String, Queue<BlockState>>(), backupMap = new HashMap<String, Queue<BlockState>>();

        originalMap.put("W", new LinkedList<BlockState>(walls));
        backupMap.put("W", new LinkedList<BlockState>(walls));

        originalMap.put("F", new LinkedList<BlockState>(flats));
        backupMap.put("F", new LinkedList<BlockState>(flats));

        originalMap.put("I", new LinkedList<BlockState>(inside));
        backupMap.put("I", new LinkedList<BlockState>(inside));

        getMapOfCages().put(playerUUID, originalMap);
        getBackupMapOfCages().put(playerUUID, backupMap);

        new BukkitRunnable() {

            public void run() {
                setCageBlocks(playerUUID);
            }

        }.runTaskLater(_plugin, 0L);

        new BukkitRunnable() {

            public void run() {
                restoreCageBlocks(playerUUID);
                getSetOfPlayersInCage().remove(playerUUID);
            }

        }.runTaskLater(_plugin, 100L);
    }

    private void setCageBlocks(UUID playerUUID) {
        Map<String, Queue<BlockState>> queueMap = getMapOfCages().get(playerUUID);
        Queue<BlockState> walls = queueMap.get("W");
        Queue<BlockState> flats = queueMap.get("F");
        Queue<BlockState> inside = queueMap.get("I");

        setBlocks(walls, _wallType);
        setBlocks(flats, _flatType);
        setBlocks(inside, _insideType);
    }

    private void setBlocks(Queue<BlockState> queue, Material material) {
        while(!queue.isEmpty()) {
            BlockState blockState = queue.poll();
            BlockState oldState = blockState;
            Location blockStateLocation = blockState.getLocation();
            int currentAmount = getMapOfCagesOnSingleLocation().containsKey(blockStateLocation)? getMapOfCagesOnSingleLocation().get(blockStateLocation) : 0;

            if (!getMapOfOldBlock().containsKey(blockStateLocation)) {
                getMapOfOldBlock().put(blockStateLocation, oldState);
            }

            blockState.getBlock().setType(material);
            getMapOfCagesOnSingleLocation().put(blockStateLocation, currentAmount + 1);
        }
    }

    private void restoreCageBlocks(UUID playerUUID) {
        Map<String, Queue<BlockState>> queueMap = getBackupMapOfCages().get(playerUUID);
        Queue<BlockState> walls = queueMap.get("W");
        Queue<BlockState> flats = queueMap.get("F");
        Queue<BlockState> inside = queueMap.get("I");

        restoreBlocks(walls);
        restoreBlocks(flats);
        restoreBlocks(inside);
    }

    private void restoreBlocks(Queue<BlockState> queue) {
        while(!queue.isEmpty()) {
            BlockState blockState = queue.poll();

            Location location = blockState.getLocation();
            int amountOfCagesOnLocation = getMapOfCagesOnSingleLocation().containsKey(location)? getMapOfCagesOnSingleLocation().get(location) : 1;

            if(amountOfCagesOnLocation == 1) {
                BlockState oldBlockState = getMapOfOldBlock().get(location);

                location.getBlock().setType(oldBlockState.getType());
                location.getBlock().setData(oldBlockState.getData().getData());
                getMapOfCagesOnSingleLocation().remove(location);
            } else {
                getMapOfCagesOnSingleLocation().put(location, amountOfCagesOnLocation - 1);
            }
        }
    }
}
