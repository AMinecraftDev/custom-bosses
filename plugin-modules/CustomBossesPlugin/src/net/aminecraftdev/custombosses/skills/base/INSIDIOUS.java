package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/**
 * Created by Charles on 27/3/2017.
 */
public class INSIDIOUS extends SkillBase implements ISkill {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        double duration = _plugin.getSkillsYML().getDouble("Skills." + getSkillType() + ".burnDuration", 2.5);

        player.setFireTicks(((int)duration) * 20);
    }
}
