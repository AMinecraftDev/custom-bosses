package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

/**
 * Created by Charles on 27/3/2017.
 */
public class KNOCKBACK extends SkillBase implements ISkill {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        double multiplier = _plugin.getSkillsYML().getDouble("Skills." + getSkillType() + ".multiplier", 5);
        Location centerLocation = location;
        Location throwLocation = player.getEyeLocation();

        double x = throwLocation.getX() - centerLocation.getX();
        double y = throwLocation.getY() - centerLocation.getY();
        double z = throwLocation.getZ() - centerLocation.getZ();

        Vector vector = new Vector(x, y, z);

        vector.normalize().multiply(multiplier);

        player.setVelocity(vector);
    }
}
