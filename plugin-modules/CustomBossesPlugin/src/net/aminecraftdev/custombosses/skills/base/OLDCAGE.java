package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by Charles on 27/3/2017.
 */
public class OLDCAGE extends SkillBase implements ISkill {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private static Map<UUID, Map<String, Queue<BlockState>>> _mapOfCages = new HashMap<UUID, Map<String, Queue<BlockState>>>();
    private static Map<UUID, Map<String, Queue<BlockState>>> _backupMap = new HashMap<UUID, Map<String, Queue<BlockState>>>();
    private Material _wallType, _flatType, _insideType;
    public static List<UUID> playersWhoAreAffected = new ArrayList<UUID>();

    @Override
    public void handleSkill(final LivingEntity player, Location location) {
        _wallType = Material.getMaterial(_plugin.getSkillsYML().getString("Skills." + getSkillType() + ".Cage.Wall").toUpperCase());
        _flatType = Material.getMaterial(_plugin.getSkillsYML().getString("Skills." + getSkillType() + ".Cage.Flat").toUpperCase());
        _insideType = Material.getMaterial(_plugin.getSkillsYML().getString("Skills." + getSkillType() + ".Cage.Inside").toUpperCase());

        if(playersWhoAreAffected.contains(player.getUniqueId())) return;

        playersWhoAreAffected.add(player.getUniqueId());

        final Location playerLocation = player.getLocation();
        final Location teleportLocation = new Location(player.getWorld(), playerLocation.getBlockX() + 0.5, playerLocation.getBlockY(), playerLocation.getBlockZ() + 0.5, playerLocation.getYaw(), playerLocation.getPitch());

        player.teleport(teleportLocation);

        final Location newPlayerLocation = player.getLocation();
        Queue<BlockState> walls = getCageWalls(newPlayerLocation);
        Queue<BlockState> flats = getCageFlats(newPlayerLocation);
        Queue<BlockState> inside = getCageInside(newPlayerLocation);
        final UUID uuid = UUID.randomUUID();
        Map<String, Queue<BlockState>> queueMap1 = new HashMap<String, Queue<BlockState>>();
        Map<String, Queue<BlockState>> queueMap2 = new HashMap<String, Queue<BlockState>>();

        queueMap1.put("W", new LinkedList<BlockState>(walls));
        queueMap2.put("W", new LinkedList<BlockState>(walls));

        queueMap1.put("F", new LinkedList<BlockState>(flats));
        queueMap2.put("F", new LinkedList<BlockState>(flats));

        queueMap1.put("I", new LinkedList<BlockState>(inside));
        queueMap2.put("I", new LinkedList<BlockState>(inside));

        _mapOfCages.put(uuid, queueMap1);
        _backupMap.put(uuid, queueMap2);

        new BukkitRunnable() {
            public void run() {
                setCageBlocks(uuid, newPlayerLocation);
            }
        }.runTaskLater(_plugin, 0L);

        new BukkitRunnable() {
            public void run() {
                restoreCageBlocks(uuid, newPlayerLocation);
                playersWhoAreAffected.remove(player.getUniqueId());
            }
        }.runTaskLater(_plugin, 100L);
    }



    private Queue<BlockState> getCageFlats(Location location) {
        World world = location.getWorld();
        Queue<BlockState> blockStateQueue = new LinkedList<BlockState>();
        Queue<Location> locationQueue = new LinkedList<Location>();

        for(int x = 1; x >= -1; x--) {
            for(int z = 1; z >= -1; z--) {
                Location location1 = new Location(world, x, +2, z);
                Location location2 = new Location(world, x, -1, z);

                locationQueue.add(location1);
                locationQueue.add(location2);
            }
        }

        locationQueue.add(new Location(world, +1, +2, -1));
        locationQueue.add(new Location(world, +1, +2, +0));

        while(!locationQueue.isEmpty()) {
            Location temp = locationQueue.poll();
            Block block = world.getBlockAt(temp.add(location).clone());

            blockStateQueue.add(block.getState());
        }

        return blockStateQueue;
    }

    private Queue<BlockState> getCageWalls(Location location) {
        World world = location.getWorld();
        Queue<BlockState> blockStateQueue = new LinkedList<BlockState>();
        Queue<Location> locationQueue = new LinkedList<Location>();

        for(int x = 1; x >= -1; x--) {
            for(int z = 1; z >= -1; z--) {
                Location location1 = new Location(world, x, 1, z);
                Location location2 = new Location(world, x, 0, z);

                locationQueue.add(location1);
                locationQueue.add(location2);
            }
        }

        while(!locationQueue.isEmpty()) {
            Location temp = locationQueue.poll();
            Block block = world.getBlockAt(temp.add(location).clone());

            blockStateQueue.add(block.getState());
        }

        return blockStateQueue;
    }

    private Queue<BlockState> getCageInside(Location location) {
        World world = location.getWorld();
        Queue<BlockState> blockStateQueue = new LinkedList<BlockState>();
        Queue<Location> locationQueue = new LinkedList<Location>();

        for(int y = 1; y >= 0; y--) {
            Location innerLocation = new Location(world, 0, y, 0);

            locationQueue.add(innerLocation);
        }

        while(!locationQueue.isEmpty()) {
            Location temp = locationQueue.poll();
            Block block = world.getBlockAt(temp.add(location).clone());

            blockStateQueue.add(block.getState());
        }

        return blockStateQueue;
    }

    private void setCageBlocks(UUID uuid, Location location) {
        Map<String, Queue<BlockState>> queueMap = _mapOfCages.get(uuid);
        Queue<BlockState> walls = queueMap.get("W");
        Queue<BlockState> flats = queueMap.get("F");
        Queue<BlockState> inside = queueMap.get("I");

        setBlocks(walls, _wallType, location);
        setBlocks(flats, _flatType, location);
        setBlocks(inside, _insideType, location);
    }

    private void setBlocks(Queue<BlockState> queue, Material material, Location location) {
        while(!queue.isEmpty()) {
            BlockState blockState = queue.poll();

            blockState.getBlock().setType(material);
            getMapOfCageLocations().put(blockState.getLocation(), location);
        }
    }

    private void restoreCageBlocks(UUID uuid, Location location) {
        Map<String, Queue<BlockState>> queueMap = _backupMap.get(uuid);
        Queue<BlockState> walls = queueMap.get("W");
        Queue<BlockState> flats = queueMap.get("F");
        Queue<BlockState> inside = queueMap.get("I");

        restoreBlocks(walls, _wallType, location);
        restoreBlocks(flats, _flatType, location);
        restoreBlocks(inside, _insideType, location);
    }

    private void restoreBlocks(Queue<BlockState> queue, Material material, Location location) {
        while(!queue.isEmpty()) {
            BlockState blockState = queue.poll();
            BlockState correctState = blockState;
            Location loc = blockState.getLocation();

            if(getMapOfCageLocations().containsKey(loc) && !getMapOfCageLocations().get(loc).equals(location)) continue;
            getMapOfCageLocations().remove(loc);

            if(!correctState.getType().equals(material)) {
                loc.getBlock().setType(correctState.getType());
                loc.getBlock().setData(correctState.getData().getData());
            }
        }
    }
}
