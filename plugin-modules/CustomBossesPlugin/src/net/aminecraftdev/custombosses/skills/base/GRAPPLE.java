package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class GRAPPLE extends SkillBase implements ISkill {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        double multiplier = _plugin.getSkillsYML().getDouble("Skills." + getSkillType() + ".multiplier", 5);
        Location playerLoc = player.getLocation();
        Vector vector = playerLoc.toVector().subtract(getLivingEntity().getLocation().toVector()).normalize().multiply(multiplier).setY(2);

        player.setVelocity(vector);
    }
}
