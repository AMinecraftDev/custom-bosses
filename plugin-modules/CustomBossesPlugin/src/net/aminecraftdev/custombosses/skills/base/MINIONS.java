package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.TargetHandler;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import net.aminecraftdev.custombosses.utils.target.MinionTargetHandler;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;

/**
 * Created by Charles on 27/3/2017.
 */
public class MINIONS extends SkillBase implements ISkill {

    private CustomBosses _plugin = CustomBosses.getInstance();

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        int amount = _plugin.getConfigYML().getInt("Settings.Skills.minionCount", 5);
        int maxMinions = _plugin.getConfigYML().getInt("Settings.Skills.maxMinionsPerBoss", Integer.MAX_VALUE);

        ConfigurationSection configurationSection = _plugin.getSkillsYML().getConfigurationSection("Skills." + getSkillType());

        for(int i = 0; i < amount; i++) {
            int currentCount = getBossUtils().getMinionCount(getLivingEntity());

            if(currentCount+1 <= maxMinions) {
                LivingEntity livingEntity = getBossUtils().spawnMinion(location, configurationSection, player, getLivingEntity().getUniqueId());
                TargetHandler targetHandler = new MinionTargetHandler(livingEntity, getLivingEntity());

                targetHandler.createAutoTarget();
            }
        }
    }
}
