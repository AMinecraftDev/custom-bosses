package net.aminecraftdev.custombosses.skills.base;

import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.utils.skill.ISkill;
import net.aminecraftdev.custombosses.utils.Messages;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

import static org.bukkit.Material.AIR;

/**
 * Created by Charles on 27/3/2017.
 */
public class DISARM extends SkillBase implements ISkill {

    @Override
    public void handleSkill(LivingEntity player, Location location) {
        int item = getRandomNumber();
        ItemStack itemStack;

        switch (item) {
            case 0:
                itemStack = player.getEquipment().getItemInHand();
                player.getEquipment().setItemInHand(new ItemStack(AIR));
                break;
            case 1:
                itemStack = player.getEquipment().getHelmet();
                player.getEquipment().setHelmet(new ItemStack(AIR));
                break;
            case 2:
                itemStack = player.getEquipment().getChestplate();
                player.getEquipment().setChestplate(new ItemStack(AIR));
                break;
            case 3:
                itemStack = player.getEquipment().getLeggings();
                player.getEquipment().setLeggings(new ItemStack(AIR));
                break;
            case 4:
            default:
                itemStack = player.getEquipment().getBoots();
                player.getEquipment().setBoots(new ItemStack(AIR));
                break;
        }

        if(itemStack == null || itemStack.getType() == AIR) return;

        location.getWorld().dropItemNaturally(player.getLocation(), itemStack);
        player.sendMessage(Messages.DISARM.toString());
    }

    private int getRandomNumber() {
        Random random = new Random();
        int id = random.nextInt(5);

        return id;
    }
}
