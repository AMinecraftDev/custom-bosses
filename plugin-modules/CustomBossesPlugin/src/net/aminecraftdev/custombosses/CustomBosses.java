package net.aminecraftdev.custombosses;

import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.utils.IInventoryManager;
import net.aminecraftdev.custombosses.utils.Messages;
import net.aminecraftdev.custombosses.zcore.c.*;
import net.aminecraftdev.custombosses.zcore.i.ICompositeRoot;
import net.aminecraftdev.custombosses.zcore.i.IPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by Charles on 8/3/2017.
 */
public class CustomBosses extends JavaPlugin implements IPlugin {

    private ICompositeRoot _compositeRoot = null;
    private static CustomBosses _inst;
    private File _autoSpawnsF, _bossesF, _configF, _dropTableF, _langF, _skillsF, _itemsF;
    private FileConfiguration _autoSpawnsC, _bossesC, _configC, _dropTableC, _langC, _skillsC, _itemsC;

    public void onEnable() {
        _inst = this;

        loadFiles();
        refreshAllFields();

        _compositeRoot.getCommandCore().registerCommands();
        _compositeRoot.getListenerCore().registerListeners();
        _compositeRoot.getAutoSpawnUtils().loadAutoSpawns();

        new MassiveStats(this);
        Debug.MCUPDATE_LOADED.out();
    }

    public void onDisable() {
        if(getConfigYML().getBoolean("Settings.KillAll.onRestart", true)) {
            _compositeRoot.getBossUtils().removeAllMinions();
            _compositeRoot.getBossUtils().removeAllBosses();
        }
    }

    public static CustomBosses getInstance() {
        return _inst;
    }

    @Override
    public void refreshAllFields() {
        if(_compositeRoot != null) {
            _compositeRoot.getBossUtils().removeAllMinions();
            _compositeRoot.getBossUtils().removeAllBosses();
            _compositeRoot.reloadFields();
        } else {
            _compositeRoot = new CompositeRoot(new NumberUtils(), new MessageUtils(), new ItemStackUtils(), new PotionUtils(), new ActionBarReflection(this), new ChunkReflection(), new EntityUtils());
        }

        Messages.setFile(getLangYML());
    }

    @Override
    public void loadFiles() {
        _autoSpawnsF = new File(getDataFolder(), "autospawns.yml");
        _bossesF = new File(getDataFolder(), "bosses.yml");
        _configF = new File(getDataFolder(), "config.yml");
        _dropTableF = new File(getDataFolder(), "droptable.yml");
        _langF = new File(getDataFolder(), "lang.yml");
        _skillsF = new File(getDataFolder(), "skills.yml");
        _itemsF = new File(getDataFolder(), "items.yml");

        if(!_autoSpawnsF.exists()) saveResource(_autoSpawnsF.getName(), false);
        if(!_bossesF.exists()) saveResource(_bossesF.getName(), false);
        if(!_configF.exists()) saveResource(_configF.getName(), false);
        if(!_dropTableF.exists()) saveResource(_dropTableF.getName(), false);
        if(!_langF.exists()) saveResource(_langF.getName(), false);
        if(!_skillsF.exists()) saveResource(_skillsF.getName(), false);
        if(!_itemsF.exists()) saveResource(_itemsF.getName(), false);

        reloadFiles();

        for(Messages m : Messages.values()) {
            if(!getLangYML().contains(m.getPath())) {
                getLangYML().set(m.getPath(), m.getDefault());
            }
        }

        try {
            _langC.save(_langF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void reloadFiles() {
        _autoSpawnsC = YamlConfiguration.loadConfiguration(_autoSpawnsF);
        _bossesC = YamlConfiguration.loadConfiguration(_bossesF);
        _configC = YamlConfiguration.loadConfiguration(_configF);
        _dropTableC = YamlConfiguration.loadConfiguration(_dropTableF);
        _langC = YamlConfiguration.loadConfiguration(_langF);
        _skillsC = YamlConfiguration.loadConfiguration(_skillsF);
        _itemsC = YamlConfiguration.loadConfiguration(_itemsF);
    }

    @Override
    public void saveFiles() {
        try {
            _configC.save(_configF);
            _bossesC.save(_bossesF);
            _langC.save(_langF);
            _skillsC.save(_skillsF);
            _dropTableC.save(_dropTableF);
            _autoSpawnsC.save(_autoSpawnsF);
            _itemsC.save(_itemsF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public FileConfiguration getAutoSpawnsYML() {
        return _autoSpawnsC;
    }

    @Override
    public FileConfiguration getBossesYML() {
        return _bossesC;
    }

    @Override
    public FileConfiguration getDropTableYML() {
        return _dropTableC;
    }

    @Override
    public FileConfiguration getLangYML() {
        return _langC;
    }

    @Override
    public FileConfiguration getSkillsYML() {
        return _skillsC;
    }

    @Override
    public FileConfiguration getConfigYML() {
        return _configC;
    }

    @Override
    public FileConfiguration getItemsYML() {
        return _itemsC;
    }

    @Override
    public IInventoryManager getInventoryManager() {
        return _compositeRoot.getInventoryManager();
    }
}
