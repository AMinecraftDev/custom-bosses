package net.aminecraftdev.custombosses.events;


import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;
import java.util.Random;

/**
 * Created by Charles on 13/3/2017.
 */
public class BossSkillEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private CustomBosses _plugin = CustomBosses.getInstance();
    private Random _random = new Random();
    private IBossUtils _bossUtils;
    private double _overallChance;
    private String _path;
    private LivingEntity _boss;
    private String _skill;
    private int _skillChance, _skillLine;
    private List<String> _skillList;
    private Player _damager;

    public BossSkillEvent(IBossUtils bossUtils, String bossType, LivingEntity boss, Player damager) {
        _bossUtils = bossUtils;
        _path = "Bosses." + bossType + ".Boss.Skills.";
        _boss = boss;
        _damager = damager;
        _skillList = _plugin.getBossesYML().getStringList(_path + "skills");

        if(_skillList.size() <= 0) return;

        _skillLine = _random.nextInt(_skillList.size());
        _skill = _skillList.get(_skillLine);
        _skillChance = _skill.contains(":")? Integer.valueOf(_skill.split(":")[0]) : 100;
        _overallChance = _plugin.getBossesYML().getDouble(_path + "overallChance", 40);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public LivingEntity getBoss() {
        return _boss;
    }

    public double getOverallSkillChance() {
        return _overallChance;
    }

    public double getSingleSkillChance() {
        return _skillChance;
    }

    public Player getDamager() {
        return _damager;
    }

    public String getBossType() {
        return _bossUtils.getBossType(_boss.getUniqueId());
    }

    public String getSkill() {
        if(_skill == null) return null;

        return _skill.contains(":")? _skill.split(":")[1] : _skill;
    }

    public String getSkillName() {
        return (_plugin.getSkillsYML().contains("Skills." + getSkill() + ".showName")? _plugin.getSkillsYML().getString("Skills." + getSkill() + ".showName") : getSkill());
    }

}