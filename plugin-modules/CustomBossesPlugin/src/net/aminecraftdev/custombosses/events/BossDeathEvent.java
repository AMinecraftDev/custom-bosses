package net.aminecraftdev.custombosses.events;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityEvent;

import java.util.Map;

/**
 * Created by Charles on 8/3/2017.
 */
public class BossDeathEvent extends EntityEvent {

    private Location _location;
    private Map<Integer, Player> _mapOfPlayerDamages;
    private ConfigurationSection bossConfSection;
    private static final HandlerList _handlers = new HandlerList();

    public BossDeathEvent(Entity what, Map<Integer, Player> mapOfPlayerDamages, Location killedLocation, ConfigurationSection configurationSection) {
        this(what, mapOfPlayerDamages, killedLocation);
        this.bossConfSection = configurationSection;
    }

    public BossDeathEvent(Entity what, Map<Integer, Player> mapOfPlayerDamages, Location killedLocation) {
        super(what);

        _location = killedLocation;
        _mapOfPlayerDamages = mapOfPlayerDamages;
        this.bossConfSection = null;
    }

    public Location getLocation() {
        return _location;
    }

    public Map<Integer, Player> getMapOfPlayerDamages() {
        return _mapOfPlayerDamages;
    }

    public ConfigurationSection getBossConfSection() {
        return this.bossConfSection;
    }

    @Override
    public HandlerList getHandlers() {
        return _handlers;
    }

    public static HandlerList getHandlerList() {
        return _handlers;
    }
}
