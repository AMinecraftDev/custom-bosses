package net.aminecraftdev.custombosses.events;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

/**
 * Created by Charles on 8/3/2017.
 */
public class BossSpawnEvent extends Event {

    private Location location;
    private LivingEntity boss;
    private CommandSender whoSpawned;
    private ConfigurationSection bossConfSection;
    private static final HandlerList handlers = new HandlerList();

    public BossSpawnEvent(CommandSender whoSpawned, Location locationSpawned, LivingEntity bossSpawned, ConfigurationSection bossConfigSection) {
        this(whoSpawned, locationSpawned, bossSpawned);
        this.bossConfSection = bossConfigSection;
    }

    public BossSpawnEvent(CommandSender whoSpawned, Location locationSpawned, LivingEntity bossSpawned) {
        this.whoSpawned = whoSpawned;
        this.boss = bossSpawned;
        this.location = locationSpawned;
        this.bossConfSection = null;
    }

    public LivingEntity getBoss() {
        return this.boss;
    }

    public Location getLocation() {
        return this.location;
    }

    public CommandSender getWhoSpawned() {
        return whoSpawned;
    }

    public ConfigurationSection getBossConfSection() {
        return bossConfSection;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
