package net.aminecraftdev.custombosses.listeners;

import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class ReTargetListener implements Listener {

    private IBossUtils _bossUtils;

    public ReTargetListener(IBossUtils bossUtils) {
        _bossUtils = bossUtils;
    }

    @EventHandler
    public void onPvE(EntityDamageByEntityEvent event) {
        Entity a1 = event.getEntity();
        Entity b1 = event.getDamager();

        if(!(b1 instanceof LivingEntity)) return;
        if(!(a1 instanceof LivingEntity)) return;

        LivingEntity a2 = (LivingEntity) event.getEntity();
        LivingEntity b2 = (LivingEntity) event.getDamager();

        if(!_bossUtils.isLivingEntityMinion(b2)) {
            if(!(b2 instanceof Creature)) return;

            Creature b3 = (Creature) b2;

            b3.setTarget(a2);
            return;
        }

        if(!_bossUtils.isLivingEntityBoss(b2)) {
            if(!(b2 instanceof Creature)) return;

            Creature b3 = (Creature) b2;

            b3.setTarget(a2);
            return;
        }
    }

}
