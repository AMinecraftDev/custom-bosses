package net.aminecraftdev.custombosses.listeners.minion;

import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

/**
 * Created by Charles on 23/3/2017.
 */
public class MinionDeathListener implements Listener {

    private IBossUtils _bossUtils;

    public MinionDeathListener(IBossUtils bossUtils) {
        _bossUtils = bossUtils;
    }

    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();

        if(!_bossUtils.isLivingEntityMinion(livingEntity)) return;

        _bossUtils.removeMinion(livingEntity);
        event.getDrops().clear();
        event.setDroppedExp(0);
    }

}
