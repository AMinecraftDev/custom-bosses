package net.aminecraftdev.custombosses.listeners;

import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.Chunk;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

/**
 * Created by Charles on 27/3/2017.
 */
public class ChunkUnloadListener implements Listener {

    private IBossUtils _bossUtils;
    private IAutoSpawnUtils _autoSpawnUtils;

    public ChunkUnloadListener(IBossUtils bossUtils, IAutoSpawnUtils autoSpawnUtils) {
        _bossUtils = bossUtils;
        _autoSpawnUtils = autoSpawnUtils;
    }

    @EventHandler
    public void onUnload(ChunkUnloadEvent event) {
        Chunk chunk = event.getChunk();

        for(Entity entity : chunk.getEntities()) {
            if(!(entity instanceof LivingEntity)) continue;

            LivingEntity livingEntity = (LivingEntity) entity;

            if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity) || _autoSpawnUtils.isLivingEntityAutoBoss(livingEntity)) {
                event.setCancelled(true);
            }
        }
    }

}
