package net.aminecraftdev.custombosses.listeners.boss.during;

import net.aminecraftdev.custombosses.events.BossSkillEvent;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * Created by Charles on 13/3/2017.
 */
public class BossDamageListener implements Listener {

    private IBossUtils _bossUtils;

    public BossDamageListener(IBossUtils bossUtils) {
        _bossUtils = bossUtils;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onDamage(EntityDamageByEntityEvent event) {
        Entity entity = event.getEntity();
        Entity damager = event.getDamager();

        if(!(entity instanceof LivingEntity)) return;

        LivingEntity livingEntity = (LivingEntity) entity;
        double damage = event.getDamage();
        Player player = null;

        if(!_bossUtils.isLivingEntityBoss(livingEntity)) return;

        if(damager instanceof Player) {
            player = (Player) damager;

        } else if(damager instanceof Projectile) {
            Projectile projectile = (Projectile) damager;
            LivingEntity shooter = (LivingEntity) projectile.getShooter();

            if(projectile instanceof ThrownPotion) {
                event.setCancelled(true);
                return;
            }
            if(!(shooter instanceof Player)) return;

            player = (Player) shooter;
        }

        if(player == null) return;

        _bossUtils.addDamageToEntity(livingEntity.getUniqueId(), player, damage);
        Bukkit.getPluginManager().callEvent(new BossSkillEvent(_bossUtils, _bossUtils.getBossType(livingEntity.getUniqueId()), livingEntity, player));
    }

}
