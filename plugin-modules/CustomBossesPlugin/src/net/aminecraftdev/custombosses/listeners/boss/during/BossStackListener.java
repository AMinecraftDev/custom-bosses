package net.aminecraftdev.custombosses.listeners.boss.during;

import net.aminecraftdev.custombosses.events.BossSpawnEvent;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import uk.antiperson.stackmob.StackMob;
import uk.antiperson.stackmob.api.EntityManager;

/**
 * Created by Charles on 13/3/2017.
 */
public class BossStackListener implements Listener {

    private IBossUtils _bossUtils;
    private EntityManager entityManager;

    public BossStackListener(IBossUtils bossUtils) {
        _bossUtils = bossUtils;

        StackMob stackMob = (StackMob) Bukkit.getPluginManager().getPlugin("StackMob");

        this.entityManager = new EntityManager(stackMob);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBossSpawn(BossSpawnEvent event) {
        LivingEntity livingEntity = event.getBoss();

        if (livingEntity == null) return;

        if (_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            this.entityManager.preventFromStacking(livingEntity);
        }
    }
}
