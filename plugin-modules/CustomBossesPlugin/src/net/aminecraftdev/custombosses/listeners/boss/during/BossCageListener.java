package net.aminecraftdev.custombosses.listeners.boss.during;

import net.aminecraftdev.custombosses.skills.SkillBase;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class BossCageListener implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();

        if(!SkillBase.getSetOfPlayersInCage().contains(player.getUniqueId())) return;

        event.setCancelled(true);
    }

}
