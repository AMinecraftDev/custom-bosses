package net.aminecraftdev.custombosses.listeners.boss.during;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.Material;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Snowman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Charles on 23/3/2017.
 */
public class BossTrailListener implements Listener {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private IBossUtils _bossUtils;
    private boolean disableEndermanBossTeleport, disablePotionEffectsOnBoss;

    public BossTrailListener(IBossUtils bossUtils) {
        _bossUtils = bossUtils;

        disableEndermanBossTeleport = _plugin.getConfigYML().getBoolean("Settings.DisableEndermanBossTeleport", true);
        disablePotionEffectsOnBoss = _plugin.getConfigYML().getBoolean("Settings.DisablePotionsAffectingBosses", true);
    }

    @EventHandler
    public void onTrail(final EntityBlockFormEvent event) {
        if(!(event.getEntity() instanceof Snowman)) return;
        if(event.getNewState().getType() != Material.SNOW) return;
        LivingEntity livingEntity = (LivingEntity) event.getEntity();

        if(_bossUtils.isLivingEntityMinion(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);

            new BukkitRunnable() {

                @Override
                public void run() {
                    event.getBlock().setType(Material.AIR);
                    return;
                }

            }.runTaskLater(_plugin, 1L);
        }
    }

    @EventHandler
    public void onTeleport(EntityTeleportEvent event) {
        Entity entity = event.getEntity();

        if(!(entity instanceof Enderman)) return;
        if(!disableEndermanBossTeleport) return;

        LivingEntity livingEntity = (LivingEntity) entity;

        if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent event) {
        if(disablePotionEffectsOnBoss) {
            event.getAffectedEntities().removeIf(entity -> ((entity instanceof LivingEntity) && (_bossUtils.isLivingEntityBoss(entity) || _bossUtils.isLivingEntityMinion(entity))));
        }

    }

}
