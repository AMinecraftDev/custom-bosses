package net.aminecraftdev.custombosses.listeners.boss.after;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.events.BossDeathEvent;
import net.aminecraftdev.custombosses.zcore.c.Debug;
import net.aminecraftdev.custombosses.utils.autospawn.IAutoSpawnUtils;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import net.aminecraftdev.custombosses.utils.IDropTableUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

/**
 * Created by Charles on 15/3/2017.
 */
public class BossDeathListener implements Listener {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private Random _random = new Random();
    private IBossUtils _bossUtils;
    private IDropTableUtils _dropTableUtils;
    private IAutoSpawnUtils _autoSpawnUtils;

    public BossDeathListener(IBossUtils bossUtils, IDropTableUtils dropTableUtils, IAutoSpawnUtils autoSpawnUtils) {
        _bossUtils = bossUtils;
        _dropTableUtils = dropTableUtils;
        _autoSpawnUtils = autoSpawnUtils;
    }

    @EventHandler
    public void onCombust(EntityCombustEvent event) {
        LivingEntity livingEntity;
        Entity entity = event.getEntity();

        if(!(entity instanceof LivingEntity)) return;

        livingEntity = (LivingEntity) entity;

        if(!_bossUtils.isLivingEntityBoss(livingEntity)) return;
        event.setCancelled(true);
    }

    @EventHandler
    public void onSplit(SlimeSplitEvent event) {
        LivingEntity livingEntity = event.getEntity();

        if(_bossUtils.isLivingEntityBoss(livingEntity)) {
            event.setCancelled(true);
            _bossUtils.removeBoss(livingEntity);
        }

        if(_bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
            _bossUtils.removeMinion(livingEntity);
        }
    }

    @EventHandler
    public void onEntityTeleport(EntityTeleportEvent event) {
        Entity entity = event.getEntity();

        if(!(entity instanceof LivingEntity)) return;
        if(entity.getType() != EntityType.ENDERMAN) return;

        LivingEntity livingEntity = (LivingEntity) entity;

        if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void preBossDeath(EntityDeathEvent event) {
        LivingEntity entity = event.getEntity();
        EntityDamageEvent entityDamageEvent = entity.getLastDamageCause();

        if(!_bossUtils.isLivingEntityBoss(entity)) return;

        if(entityDamageEvent.getCause() == EntityDamageEvent.DamageCause.VOID ||
                entityDamageEvent.getCause() == EntityDamageEvent.DamageCause.LAVA) {
            _bossUtils.removeBoss(entity);
        }
    }

    @EventHandler
    public void onBossDeathStep1(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();
        EntityDamageEvent entityDamageEvent = livingEntity.getLastDamageCause();

        if(!_bossUtils.isLivingEntityBoss(livingEntity)) return;
        if(entityDamageEvent.getCause() == EntityDamageEvent.DamageCause.VOID || entityDamageEvent.getCause() == EntityDamageEvent.DamageCause.LAVA) return;

        List<String> listOfDamages = _bossUtils.getMapOfDamages(livingEntity.getUniqueId());
        String type = _bossUtils.getBossType(livingEntity.getUniqueId());
        ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses." + type);

        if(listOfDamages == null) return;

        _bossUtils.onDeathBroadcastMessage(livingEntity.getLocation(), listOfDamages, livingEntity.getUniqueId(), livingEntity.getWorld(), livingEntity);
        Bukkit.getPluginManager().callEvent(new BossDeathEvent(livingEntity, _bossUtils.getMapOfBossKillers(livingEntity.getUniqueId()), livingEntity.getLocation(), configurationSection));
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBossDeathStep2(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();
        EntityDamageEvent entityDamageEvent = livingEntity.getLastDamageCause();

        if(!_bossUtils.isLivingEntityBoss(livingEntity)) return;
        if(entityDamageEvent.getCause() == EntityDamageEvent.DamageCause.VOID || entityDamageEvent.getCause() == EntityDamageEvent.DamageCause.LAVA) return;

        String type = _bossUtils.getBossType(livingEntity.getUniqueId());
        ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses." + type);
        ConfigurationSection dropSection = configurationSection.getConfigurationSection("Drops");
        String dropTable = dropSection.getString("DropTable.table");
        boolean isDropTableEnabled = dropSection.getBoolean("DropTable.enabled");
        boolean doNaturalDrops = dropSection.getBoolean("NaturalDrops");

        if(!doNaturalDrops) {
            event.getDrops().clear();
            event.setDroppedExp(0);
        }

        if(!dropSection.contains("DropTable")) {
            Debug.Boss_NotLatestDropTable.out(type);
            Debug.Boss_DeleteOldFiles.out();
            return;
        }

        if(isDropTableEnabled) {
            _dropTableUtils.handleDropTable(dropTable, livingEntity.getUniqueId());
            return;
        }

        Debug.Boss_NotUsingDropTable.out();

        Player topDamager = _bossUtils.getMapOfBossKillers(livingEntity.getUniqueId()).get(1);

        List<ItemStack> customDrops = _dropTableUtils.getBossCustomDrops(dropSection);
        boolean randomCustomDrop = dropSection.getBoolean("RandomCustomDrop");

        if(randomCustomDrop) {
            if(customDrops.size() <= 0) return;

            int num = _random.nextInt(customDrops.size());

            topDamager.getLocation().getWorld().dropItemNaturally(topDamager.getLocation(), customDrops.get(num));
        } else {
            for(ItemStack itemStack : customDrops) {
                topDamager.getLocation().getWorld().dropItemNaturally(topDamager.getLocation(), itemStack);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBossDeathStep3(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();

        if(!_bossUtils.isLivingEntityBoss(livingEntity)) return;

        if(_autoSpawnUtils.isLivingEntityAutoBoss(livingEntity)) {
            _bossUtils.removeAutoBoss(livingEntity);
        } else {
            _bossUtils.removeBoss(livingEntity);
        }
    }

}
