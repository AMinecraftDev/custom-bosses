package net.aminecraftdev.custombosses.listeners.boss.during;

import net.aminecraftdev.custombosses.utils.IBossUtils;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;

/**
 * Created by Charles on 23/3/2017.
 */
public class BossWitherListener implements Listener {

    private IBossUtils _bossUtils;

    public BossWitherListener(IBossUtils bossUtils) {
        _bossUtils = bossUtils;
    }

    @EventHandler
    public void onBreak(EntityChangeBlockEvent event) {
        EntityType entityType = event.getEntityType();

        if(entityType == null || entityType != EntityType.WITHER) return;
        LivingEntity livingEntity = (LivingEntity) event.getEntity();

        if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPaintingBreak(HangingBreakByEntityEvent event) {
        EntityType entityType = event.getRemover().getType();

        if(entityType == null || entityType != EntityType.WITHER) return;
        LivingEntity livingEntity = (LivingEntity) event.getRemover();

        if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onExplosion(EntityExplodeEvent event) {
        if(event.getEntityType() == null) return;

        EntityType entityType = event.getEntityType();

        if(entityType == null || entityType != EntityType.WITHER) return;
        LivingEntity livingEntity = (LivingEntity) event.getEntity();

        if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onExplode(ExplosionPrimeEvent event) {
        EntityType entityType = event.getEntityType();

        if(entityType == null || entityType != EntityType.WITHER) return;
        LivingEntity livingEntity = (LivingEntity) event.getEntity();

        if(_bossUtils.isLivingEntityBoss(livingEntity) || _bossUtils.isLivingEntityMinion(livingEntity)) {
            event.setCancelled(true);
        }
    }

}
