package net.aminecraftdev.custombosses.listeners.boss.pre;

import net.aminecraftdev.custombosses.events.BossSpawnEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class BossSpawnCommandListener implements Listener {

    @EventHandler
    public void onSpawn(BossSpawnEvent event) {
        if(event.getBossConfSection() == null) return;

        ConfigurationSection configurationSection = event.getBossConfSection();

        if(!configurationSection.contains("Commands")) return;
        if(!configurationSection.contains("Commands.onSpawn")) return;

        List<String> commands = configurationSection.getStringList("Commands.onSpawn");

        for(String s : commands) {
            if(event.getWhoSpawned() instanceof Player) {
                s = s.replace("{player}", event.getWhoSpawned().getName());
            }

            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s);
        }
    }

}
