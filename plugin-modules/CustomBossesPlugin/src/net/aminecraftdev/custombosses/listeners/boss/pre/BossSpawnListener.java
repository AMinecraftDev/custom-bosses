package net.aminecraftdev.custombosses.listeners.boss.pre;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.events.BossSpawnEvent;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import net.aminecraftdev.custombosses.utils.ILocationManager;
import net.aminecraftdev.custombosses.utils.Messages;
import net.aminecraftdev.custombosses.utils.TargetHandler;
import net.aminecraftdev.custombosses.utils.target.BossTargetHandler;
import net.aminecraftdev.custombosses.zcore.i.IItemStackUtils;
import net.aminecraftdev.custombosses.zcore.i.IMessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Charles on 13/3/2017.
 */
public class BossSpawnListener implements Listener {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private ILocationManager _locationManager;
    private IItemStackUtils itemStackUtils;
    private IBossUtils _bossUtils;

    public BossSpawnListener(ILocationManager locationManager, IMessageUtils messageUtils, IBossUtils bossUtils, IItemStackUtils itemStackUtils) {
        _locationManager = locationManager;
        _bossUtils = bossUtils;
        this.itemStackUtils = itemStackUtils;
    }

    @EventHandler
    public void onBossSpawn(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        BlockFace blockFace = event.getBlockFace();
        Action action = event.getAction();
        String bossType = null;

        if(!event.hasItem()) return;
        if(action != Action.RIGHT_CLICK_BLOCK) return;
        if(block.getType() == Material.AIR) return;

        ItemStack itemStack = player.getItemInHand().clone();

        boolean isABoss = false;
        ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection("Bosses");

        for(String boss : configurationSection.getKeys(false)) {
            bossType = boss;

            ItemStack spawnItem = this.itemStackUtils.createItemStack(configurationSection.getConfigurationSection(boss + ".Item"));

            if((spawnItem.getType() == itemStack.getType()) && (spawnItem.hasItemMeta() == itemStack.hasItemMeta()) && (spawnItem.getItemMeta().equals(itemStack.getItemMeta()))) {
                isABoss = true;
                break;
            }
        }

        if(!isABoss) return;
        if(bossType == null) return;

        Location location = block.getLocation().clone();

        if(blockFace == BlockFace.UP) {
            location.add(0,1,0);
        }

        if(!_locationManager.canSpawnBoss(player, location)) {
            event.setCancelled(true);
            Messages.CANNOT_SPAWN.msg(player);
            return;
        }

        ConfigurationSection bossSection = configurationSection.getConfigurationSection(bossType);
        LivingEntity boss = _bossUtils.spawnBoss(location, bossType, bossSection);

        if(boss == null) return;

        TargetHandler targetHandler = new BossTargetHandler(boss);

        _bossUtils.onSpawnBroadcastMessage(location, bossType, player.getWorld());
        itemStack.setAmount(1);
        event.setCancelled(true);
        player.getInventory().removeItem(itemStack);
        player.updateInventory();
        targetHandler.createAutoTarget();

        Bukkit.getPluginManager().callEvent(new BossSpawnEvent(player, location, boss, bossSection));
    }

}
