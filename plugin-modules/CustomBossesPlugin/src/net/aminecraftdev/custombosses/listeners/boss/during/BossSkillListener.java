package net.aminecraftdev.custombosses.listeners.boss.during;

import net.aminecraftdev.custombosses.events.BossSkillEvent;
import net.aminecraftdev.custombosses.skills.SkillBase;
import net.aminecraftdev.custombosses.skills.SkillHandler;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import net.aminecraftdev.custombosses.utils.skill.ISkillHandler;
import net.aminecraftdev.custombosses.utils.skill.ISkillType;
import net.aminecraftdev.custombosses.utils.skill.ISkillUtils;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;

import java.util.List;

/**
 * Created by Charles on 27/3/2017.
 */
public class BossSkillListener implements Listener {

    private ISkillUtils _skillUtils;

    public BossSkillListener(ISkillUtils skillUtils, IBossUtils bossUtils) {
        _skillUtils = skillUtils;

        SkillBase.setBossUtils(bossUtils);
    }

    @EventHandler
    public void onSkill(BossSkillEvent event) {
        if(event.getSkill() == null) return;

        String bossType = event.getBossType();
        Player damager = event.getDamager();
        LivingEntity boss = event.getBoss();
        double overallSkillChance = event.getOverallSkillChance();
        double singleSkillChance = event.getSingleSkillChance();
        String skill = event.getSkillName();

        if(!_skillUtils.isSkillRunnable(overallSkillChance)) return;
        if(!_skillUtils.isSkillRunnable(singleSkillChance)) return;

        ISkillHandler skillHandler = new SkillHandler(event.getSkill(), boss, damager);

        if(skillHandler.getSkillType() == null) return;

        ISkillType skillType = skillHandler.getSkillType().getSkillType();
        List<LivingEntity> affectedPlayers = skillHandler.getListOfTargettedPlayers();

        skillType.runSkill(affectedPlayers, skillHandler.getSkill(), boss);
        _skillUtils.onSkillMessage(affectedPlayers, bossType, boss, skill);

    }

}
