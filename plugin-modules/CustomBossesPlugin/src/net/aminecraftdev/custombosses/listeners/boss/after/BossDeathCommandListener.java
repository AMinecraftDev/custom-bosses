package net.aminecraftdev.custombosses.listeners.boss.after;

import net.aminecraftdev.custombosses.events.BossDeathEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

/**
 * @author AMinecraftDev
 * @version 2.9.17-STABLE
 * @since 15-May-17
 */
public class BossDeathCommandListener implements Listener {

    @EventHandler
    public void onDeath(BossDeathEvent event) {
        if(event.getBossConfSection() == null) return;

        ConfigurationSection configurationSection = event.getBossConfSection();

        if(!configurationSection.contains("Commands")) return;
        if(!configurationSection.contains("Commands.onDeath")) return;

        List<String> commands = configurationSection.getStringList("Commands.onDeath");

        for(String s : commands) {
            if(s.contains("{player}")) {
                s = s.replace("{player}", event.getMapOfPlayerDamages().get(1).getName());
            }

            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s);
        }
    }

}
