package net.aminecraftdev.custombosses.listeners.players;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.utils.IBossUtils;
import net.aminecraftdev.custombosses.zcore.i.IActionBarReflection;
import net.aminecraftdev.custombosses.zcore.i.IMessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by Charles on 25/3/2017.
 */
public class PlayerMoveListener implements Listener {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private Set<UUID> _uuidsInCooldown = new HashSet<UUID>();
    private IBossUtils _bossUtils;
    private IActionBarReflection _actionBarReflection;
    private IMessageUtils _messageUtils;
    private String _actionBarMessage;
    private boolean _actionBarEnabled;

    public PlayerMoveListener(IBossUtils bossUtils, IActionBarReflection actionBarReflection, IMessageUtils messageUtils) {
        _bossUtils = bossUtils;
        _actionBarReflection = actionBarReflection;
        _messageUtils = messageUtils;
        _actionBarEnabled = _plugin.getConfigYML().getBoolean("Settings.ActionBar.enabled", true);
        _actionBarMessage = _plugin.getConfigYML().getString("Settings.ActionBar.healthMessage");
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Location to = event.getTo();
        Location from = event.getFrom();
        final Player player = event.getPlayer();
        Location location = player.getLocation();

        if(!_actionBarEnabled) return;
        if(_uuidsInCooldown.contains(player.getUniqueId())) return;
        if((to.getBlockX() == from.getBlockX()) && (to.getBlockY() == from.getBlockY()) && (to.getBlockZ() == from.getBlockZ())) return;
        if(_bossUtils.getClosestBoss(location) == null) return;
        LivingEntity closestBoss = _bossUtils.getClosestBoss(location);

        if(closestBoss.isDead()) return;

        String string = _actionBarMessage;
        string = string.replace("{boss}", _bossUtils.getBossName(closestBoss.getUniqueId()));
        string = string.replace("{current}", ""+(int)closestBoss.getHealth());
        string = string.replace("{max}", ""+(int)closestBoss.getMaxHealth());

        _actionBarReflection.sendActionBar(player, _messageUtils.translateString(string), 80);
        _uuidsInCooldown.add(player.getUniqueId());

        new BukkitRunnable() {

            public void run() {
                _uuidsInCooldown.remove(player.getUniqueId());
                cancel();
            }

        }.runTaskLater(_plugin, 60L);
    }
}
