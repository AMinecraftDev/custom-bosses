package net.aminecraftdev.custombosses.listeners;

import net.aminecraftdev.custombosses.CustomBosses;
import net.aminecraftdev.custombosses.zcore.i.IInventoryUtils;
import net.aminecraftdev.custombosses.zcore.i.IItemStackUtils;
import net.aminecraftdev.custombosses.zcore.i.IMessageUtils;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by charl on 4/1/2017.
 */
public class GUIListener implements Listener {

    private CustomBosses _plugin = CustomBosses.getInstance();
    private Map<UUID, Integer> _mapOfOpenPages = new HashMap<UUID, Integer>();
    private String _name;
    private int _prevPage, _nextPage;
    private IItemStackUtils _itemStackUtils;

    public GUIListener(IMessageUtils messageUtils, IItemStackUtils itemStackUtils) {
        _itemStackUtils = itemStackUtils;
        _prevPage = -1;
        _nextPage = -1;
        _name = messageUtils.translateString(_plugin.getConfigYML().getString("BossGUI.name"));

        ConfigurationSection configurationSection = _plugin.getConfigYML().getConfigurationSection("BossGUI.Items");

        for(String s : configurationSection.getKeys(false)) {
            if(configurationSection.contains(s + ".NextPage") && configurationSection.getBoolean(s + ".NextPage")) {
                _nextPage = Integer.valueOf(s) - 1;

                if((_nextPage != -1) && (_prevPage != -1)) break;
            }

            if(configurationSection.contains(s + ".PreviousPage") && configurationSection.getBoolean(s + ".PreviousPage")) {
                _prevPage = Integer.valueOf(s) - 1;

                if((_nextPage != -1) && (_prevPage != -1)) break;
            }
        }
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        Inventory inventory = event.getInventory();
        HumanEntity player = event.getPlayer();

        if(!inventory.getName().equals(_name)) return;
        if(_mapOfOpenPages.containsKey(player.getUniqueId())) return;

        _mapOfOpenPages.put(player.getUniqueId(), 1);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Inventory inventory = event.getInventory();
        HumanEntity player = event.getWhoClicked();
        int slot = event.getSlot();
        Map<Integer, IInventoryUtils> map = _plugin.getInventoryManager().getMapOfPages();

        if(!inventory.getName().equals(_name)) return;

        event.setCancelled(true);

        if(!_mapOfOpenPages.containsKey(player.getUniqueId())) {
            player.closeInventory();
            return;
        }

        int playerCurrentPage = _mapOfOpenPages.get(player.getUniqueId());

        if(slot == _prevPage) {
            int previousNumber = playerCurrentPage - 1;

            if(map.containsKey(previousNumber)) {
                player.openInventory(map.get(previousNumber).getInventory());
                _mapOfOpenPages.put(player.getUniqueId(), previousNumber);
                return;
            }
        }

        if(slot == _nextPage) {
            int nextNumber = playerCurrentPage + 1;

            if(map.containsKey(nextNumber)) {
                player.openInventory(map.get(nextNumber).getInventory());
                _mapOfOpenPages.put(player.getUniqueId(), nextNumber);
                return;
            }
        }

        if(event.getInventory() != player.getOpenInventory().getTopInventory()) return;
        if(event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR) return;

        String egg = _plugin.getInventoryManager().getMapOfEggs().get(playerCurrentPage).get(slot);

        if(egg == null)return;

        String path = "Bosses." + egg + ".Item";
        ConfigurationSection configurationSection = _plugin.getBossesYML().getConfigurationSection(path);
        ItemStack itemStack = _itemStackUtils.createItemStack(configurationSection, 1, null);

        player.getInventory().addItem(itemStack);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        final HumanEntity humanEntity = event.getPlayer();
        Inventory inventory = event.getInventory();

        if(!_mapOfOpenPages.containsKey(humanEntity.getUniqueId())) return;
        if(!inventory.getName().equals(_name)) return;

        new BukkitRunnable() {
            public void run() {
                if(humanEntity.getOpenInventory().getTopInventory().getName().equals(_name)) return;

                _mapOfOpenPages.remove(humanEntity.getUniqueId());
            }
        }.runTaskLater(_plugin, 5L);
    }

}
