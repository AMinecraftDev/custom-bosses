package net.aminecraftdev.custombosses.zcore.c;

import com.wasteofplastic.askyblock.ASkyBlock;
import com.wasteofplastic.askyblock.Island;
import org.bukkit.entity.Player;

/**
 * @author Charles Cullen
 * @version 1.0.0
 * @since 09-Apr-18
 */
public class aSkyblock {

    public boolean isOnOwnIsland(Player player) {
        Island island = ASkyBlock.getPlugin().getGrid().getProtectedIslandAt(player.getLocation());

        if(island == null) return false;

        return island.getMembers().contains(player.getUniqueId());
    }

    public boolean isOnAnIsland(Player player) {
        Island island = ASkyBlock.getPlugin().getGrid().getProtectedIslandAt(player.getLocation());

        return island != null;
    }

}
