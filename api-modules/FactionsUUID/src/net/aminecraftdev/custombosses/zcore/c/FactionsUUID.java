package net.aminecraftdev.custombosses.zcore.c;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Relation;
import net.aminecraftdev.custombosses.zcore.i.IFactions;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * Created by Charles on 13/3/2017.
 */
public class FactionsUUID implements IFactions {

    public boolean isFriendly(Player a, Player b) {
        Faction pFac = FPlayers.getInstance().getByPlayer(a).getFaction();
        Faction zFac = FPlayers.getInstance().getByPlayer(b).getFaction();
        Relation r = pFac.getRelationTo(zFac);

        if(ChatColor.stripColor(zFac.getId()).equalsIgnoreCase("Wilderness")) return false;
        if(a == b) return true;
        if(r.isEnemy()) return false;
        if(r.isNeutral()) return false;
        if(r.isTruce()) return true;
        if(r.isAlly()) return true;
        if(r.isMember()) return true;
        return false;
    }

    public boolean isFriendly(Player a, Location location) {
        Faction pFac = FPlayers.getInstance().getByPlayer(a).getFaction();
        FLocation fLoc = new FLocation(location);
        Faction locFac = Board.getInstance().getFactionAt(fLoc);
        Relation r = pFac.getRelationTo(locFac);

        if(ChatColor.stripColor(locFac.getComparisonTag()).equalsIgnoreCase("Wilderness")) return false;
        if(r.isEnemy()) return false;
        if(r.isNeutral()) return false;
        if(r.isTruce()) return true;
        if(r.isAlly()) return true;
        if(r.isMember()) return true;
        return false;
    }

    public boolean isInWarzone(Location location) {
        FLocation fLoc = new FLocation(location);
        Faction locFac = Board.getInstance().getFactionAt(fLoc);

        if(ChatColor.stripColor(locFac.getComparisonTag()).equalsIgnoreCase("WarZone")) return true;
        return false;
    }

    public boolean isInClaimedLand(Location location) {
        FLocation fLoc = new FLocation(location);
        Faction locFac = Board.getInstance().getFactionAt(fLoc);
        String string = ChatColor.stripColor(locFac.getComparisonTag());

        if(string.equalsIgnoreCase("WarZone")) return false;
        if(string.equalsIgnoreCase("SafeZone")) return false;
        if(string.equalsIgnoreCase("Wilderness")) return false;
        return true;
    }
}
