package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IFactions;
import net.redstoneore.legacyfactions.Relation;
import net.redstoneore.legacyfactions.entity.Board;
import net.redstoneore.legacyfactions.entity.FPlayer;
import net.redstoneore.legacyfactions.entity.FPlayerColl;
import net.redstoneore.legacyfactions.entity.Faction;
import net.redstoneore.legacyfactions.locality.Locality;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 12-Oct-17
 */
public class LegacyFactions implements IFactions {

    @Override
    public boolean isFriendly(Player a, Player b) {
        FPlayer fPlayer1 = FPlayerColl.get(a);
        FPlayer fPlayer2 = FPlayerColl.get(b);
        Relation relation = fPlayer1.getRelationTo(fPlayer2);

        if(fPlayer2.getFaction().isWilderness() || relation == Relation.ENEMY || relation == Relation.NEUTRAL) return false;
        if(relation == Relation.ALLY) return true;
        if(relation == Relation.TRUCE) return false;
        if(relation == Relation.MEMBER) return true;
        return false;
    }

    @Override
    public boolean isFriendly(Player a, Location location) {
        FPlayer fPlayer = FPlayerColl.get(a);
        Faction fPlayerFaction = fPlayer.getFaction();
        Faction locationFaction = Board.get().getFactionAt(Locality.of(location));
        Relation relation = fPlayerFaction.getRelationTo(locationFaction);

        if(locationFaction.isWilderness()) return true;
        if(relation == Relation.ENEMY || relation == Relation.NEUTRAL) return false;
        if(relation == Relation.ALLY || relation == Relation.TRUCE || relation == Relation.MEMBER) return true;
        return false;
    }

    @Override
    public boolean isInWarzone(Location location) {
        Faction faction = Board.get().getFactionAt(Locality.of(location));

        return faction.isWarZone();
    }

    @Override
    public boolean isInClaimedLand(Location location) {
        Faction faction = Board.get().getFactionAt(Locality.of(location));

        if(faction.isWarZone() || faction.isSafeZone() || faction.isWilderness()) return false;
        return true;
    }
}
