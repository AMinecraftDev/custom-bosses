package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IPotionUtils;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Charles on 7/3/2017.
 */
public class PotionUtils implements IPotionUtils {

    @Override
    public PotionEffect getPotionEffect(ConfigurationSection configurationSection) {
        int level = configurationSection.getInt("level");
        int duration = configurationSection.getInt("duration");
        String type = configurationSection.getString("type");

        if(PotionEffectType.getByName(type) == null) {
            Debug.Boss_NullPotion.out(type);
            return null;
        }

        PotionEffectType potionEffectType = PotionEffectType.getByName(type);

        if(duration == -1) {
            duration = 100000;
        } else {
            duration *= 20;
        }

        level -= 1;

        return new PotionEffect(potionEffectType, duration, level);
    }

}
