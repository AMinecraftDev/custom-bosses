package net.aminecraftdev.custombosses.zcore.c;


import net.aminecraftdev.custombosses.zcore.i.INumberUtils;

import java.text.DecimalFormat;

/**
 * Created by Charles on 7/3/2017.
 */
public class NumberUtils implements INumberUtils {

    @Override
    public String formatDouble(double d) {
        DecimalFormat format = new DecimalFormat("###,###,###,###,###.##");

        return format.format(d);
    }

    @Override
    public String formatTime(int time) {
        int hours = time / 3600;
        int remainder = time % 3600;
        int minutes = remainder / 60;
        int seconds = remainder % 60;
        String disHour = (hours < 10 ? "0" : "") + hours;
        String disMinu = (minutes < 10 ? "0" : "") + minutes;
        String disSeco = (seconds < 10 ? "0" : "") + seconds;
        String formatted = "";

        if(hours != 0) formatted += disHour + " hours ";
        if(minutes != 0) formatted += disMinu + " minutes ";
        if(seconds != 0) formatted += disSeco + " seconds.";

        return formatted;
    }

    @Override
    public boolean isStringInteger(String s) {
        try {
            Integer.valueOf(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    @Override
    public boolean isStringDouble(String s) {
        try {
            Double.valueOf(s);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

}
