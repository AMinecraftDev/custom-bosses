package net.aminecraftdev.custombosses.zcore.c;

import org.bukkit.inventory.ItemStack;

/**
 * @author AMinecraftDev
 * @version 1.0.0
 * @since 08-Jun-17
 */
public class GlowEnchant {

    public static ItemStack addGlow(ItemStack base) {
        ItemStack craftStack = NbtFactory.getCraftItemStack(base);
        NbtFactory.NbtCompound compound = NbtFactory.fromItemTag(craftStack);

        compound.put("ench", NbtFactory.createList());

        return craftStack;
//        ItemMeta itemMeta = base.getItemMeta();
//
//        itemMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
//        base.setItemMeta(itemMeta);
//        base.addUnsafeEnchantment(Enchantment.OXYGEN, 3);
//
//        return base;
    }

}
