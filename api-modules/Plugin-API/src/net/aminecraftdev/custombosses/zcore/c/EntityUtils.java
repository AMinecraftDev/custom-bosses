package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IEntityUtils;
import org.bukkit.Location;
import org.bukkit.entity.*;

import java.security.Guard;

/**
 * Created by charl on 4/3/2017.
 */
public class EntityUtils extends ReflectionUtils implements IEntityUtils {

    @Override
    public LivingEntity getWitherSkeleton(Location location) {
        if(getAPIVersion().startsWith("v1_11_R") || getAPIVersion().startsWith("v1_12_R")) {
            return (LivingEntity) location.getWorld().spawn(location, EntityType.WITHER_SKELETON.getEntityClass());
        } else {
            Skeleton skeleton = (Skeleton) location.getWorld().spawn(location, EntityType.SKELETON.getEntityClass());
            skeleton.setSkeletonType(Skeleton.SkeletonType.WITHER);

            return skeleton;
        }
    }

    @Override
    public LivingEntity getElderGuardian(Location location) {
        if(getAPIVersion().startsWith("v1_11_R") || getAPIVersion().startsWith("v1_12_R")) {
            return (LivingEntity) location.getWorld().spawn(location, EntityType.ELDER_GUARDIAN.getEntityClass());
        } else {
            Guardian guardian = (Guardian) location.getWorld().spawn(location, EntityType.GUARDIAN.getEntityClass());
            guardian.setElder(true);

            return guardian;
        }
    }

    @Override
    public LivingEntity getKillerBunny(Location location) {
        Rabbit rabbit = (Rabbit) location.getWorld().spawn(location, EntityType.RABBIT.getEntityClass());
        rabbit.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);

        return rabbit;
    }

    @Override
    public LivingEntity getBabyZombie(Location location) {
        Zombie zombie = (Zombie)  location.getWorld().spawn(location, EntityType.ZOMBIE.getEntityClass());
        zombie.setBaby(true);

        return zombie;
    }

    @Override
    public LivingEntity getZombie(Location location) {
        Zombie zombie = (Zombie)  location.getWorld().spawn(location, EntityType.ZOMBIE.getEntityClass());
        zombie.setBaby(false);

        return zombie;
    }

    @Override
    public LivingEntity getBabyPigZombie(Location location) {
        PigZombie pigZombie = (PigZombie)  location.getWorld().spawn(location, EntityType.PIG_ZOMBIE.getEntityClass());
        pigZombie.setBaby(true);

        return pigZombie;
    }

    @Override
    public LivingEntity getPigZombie(Location location) {
        PigZombie pigZombie = (PigZombie)  location.getWorld().spawn(location, EntityType.PIG_ZOMBIE.getEntityClass());
        pigZombie.setBaby(false);

        return pigZombie;
    }

    @Override
    public LivingEntity getSlime(Location location, int size) {
        Slime slime = (Slime) location.getWorld().spawn(location, EntityType.SLIME.getEntityClass());
        slime.setSize(size);

        return slime;
    }

    @Override
    public LivingEntity getMagmaCube(Location location, int size) {
        MagmaCube magmaCube = (MagmaCube) location.getWorld().spawn(location, EntityType.MAGMA_CUBE.getEntityClass());
        magmaCube.setSize(size);

        return magmaCube;
    }

    @Override
    public LivingEntity getChargedCreeper(Location location) {
        Creeper creeper = (Creeper) location.getWorld().spawn(location, EntityType.CREEPER.getEntityClass());
        creeper.setPowered(true);

        return creeper;
    }

}
