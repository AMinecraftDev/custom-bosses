package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IReflectionUtils;
import org.bukkit.Bukkit;

/**
 * Created by Charles on 7/3/2017.
 */
public class ReflectionUtils implements IReflectionUtils {

    private String _nmsVersion;

    public ReflectionUtils() {
        _nmsVersion = Bukkit.getServer().getClass().getPackage().getName();
        _nmsVersion = _nmsVersion.substring(_nmsVersion.lastIndexOf(".") + 1);
    }

    @Override
    public String getAPIVersion() {
        return _nmsVersion;
    }

    @Override
    public Class<?> getNMSClass(String name) {
        try {
            return Class.forName("net.minecraft.server." + _nmsVersion + "." + name);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Class<?> getOBCClass(String name) {
        try {
            return Class.forName("org.bukkit.craftbukkit." + _nmsVersion + "." + name);
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

}
