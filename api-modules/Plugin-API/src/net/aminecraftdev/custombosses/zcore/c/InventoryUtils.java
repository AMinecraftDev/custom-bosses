package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IInventoryUtils;
import net.aminecraftdev.custombosses.zcore.i.IItemStackUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * Created by Charles on 7/3/2017.
 */
public class InventoryUtils implements IInventoryUtils {

    private String _name;
    private int _slots;
    private ConfigurationSection _itemConfigSection;
    private Inventory _inventory;
    private IItemStackUtils _itemStackUtils;
    private HashMap<String, String> _replacedMap = new HashMap<String, String>();

    public InventoryUtils(ConfigurationSection configurationSection, IItemStackUtils iItemStackUtils) {
        defaultConstructor(configurationSection, iItemStackUtils);
    }

    public InventoryUtils(ConfigurationSection configurationSection, IItemStackUtils iItemStackUtils, HashMap<String, String> replacedMap) {
        defaultConstructor(configurationSection, iItemStackUtils);
        _replacedMap = replacedMap;
    }

    private void defaultConstructor(ConfigurationSection configurationSection, IItemStackUtils iItemStackUtils) {
        _name = ChatColor.translateAlternateColorCodes('&', configurationSection.getString("name"));
        _slots = configurationSection.getInt("slots");
        _itemConfigSection = configurationSection.getConfigurationSection("Items");
        _itemStackUtils = iItemStackUtils;
    }

    @Override
    public void createInventory() {
        _inventory = Bukkit.createInventory(null, _slots, _name);

        for(String s : _itemConfigSection.getKeys(false)) {
            int slot = Integer.valueOf(s) - 1;

            if(!_itemConfigSection.contains(s + ".type")) continue;

            ItemStack itemStack = _itemStackUtils.createItemStack(_itemConfigSection.getConfigurationSection(s), 1, _replacedMap);
            _inventory.setItem(slot, itemStack);
        }
    }

    @Override
    public Inventory getInventory() {
        return _inventory;
    }

    @Override
    public void addReplacedMap(HashMap<String, String> map) {
        _replacedMap.putAll(map);
    }

}
