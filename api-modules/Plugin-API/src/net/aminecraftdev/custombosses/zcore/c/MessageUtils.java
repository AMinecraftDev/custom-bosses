package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IMessageUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Charles on 20/1/2017.
 */
public class MessageUtils implements IMessageUtils {

    private final static int CENTER_PX = 154;
    private final static int MAX_PX = 250;

    @Override
    public int getMaxPixel() {
        return MAX_PX;
    }

    @Override
    public List<String> getCenteredMessage(String message) {
        if(message == null || message.equals("")) return new ArrayList<String>();
        List<String> arrayList = new ArrayList<String>();

        message = translateString(message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;
        int charIndex = 0;
        int lastSpaceIndex = 0;
        String toSendAfter = null;
        String recentColorCode = "";

        for(char c : message.toCharArray()) {
            if(c == '§') {
                previousCode = true;
                continue;
            } else if(previousCode) {
                previousCode = false;

                if(c == 'l' || c == 'L') {
                    isBold = true;
                    continue;
                } else {
                    isBold = false;
                }
            } else if(c == ' ') {
                lastSpaceIndex = charIndex;
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);

                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }

            if(messagePxSize >= MAX_PX) {
                toSendAfter = recentColorCode + message.substring(lastSpaceIndex + 1, message.length());
                message = message.substring(0, lastSpaceIndex + 1);
                break;
            }

            charIndex++;
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder stringBuilder = new StringBuilder();

        while (compensated < toCompensate) {
            stringBuilder.append(" ");
            compensated += spaceLength;
        }

        String s = stringBuilder.toString() + message;
        arrayList.add(s);

        if(toSendAfter != null) {
            return null;
        }

        return arrayList;
    }

    @Override
    public void sendCenteredMessageV1(CommandSender player, String message) {
        if(message == null || message.equals("")) player.sendMessage("");
        message = ChatColor.translateAlternateColorCodes('&', message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;

        for(char c : message.toCharArray()){
            if(c == '§'){
                previousCode = true;
                continue;
            }else if(previousCode == true){
                previousCode = false;
                if(c == 'l' || c == 'L'){
                    isBold = true;
                    continue;
                }else isBold = false;
            }else{
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder sb = new StringBuilder();
        while(compensated < toCompensate){
            sb.append(" ");
            compensated += spaceLength;
        }

        player.sendMessage(sb.toString() + message);
    }

    @Override
    public void sendCenteredMessageV2(CommandSender player, String message) {
        if(message == null || message.equals("")) player.sendMessage("");

        message = translateString(message);

        int messagePxSize = 0;
        boolean previousCode = false;
        boolean isBold = false;
        int charIndex = 0;
        int lastSpaceIndex = 0;
        String toSendAfter = null;
        String recentColorCode = "";

        for(char c : message.toCharArray()) {
            if(c == '§') {
                previousCode = true;
                continue;
            } else if(previousCode) {
                previousCode = false;

                if(c == 'l' || c == 'L') {
                    isBold = true;
                    continue;
                } else {
                    isBold = false;
                }
            } else if(c == ' ') {
                lastSpaceIndex = charIndex;
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);

                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }

            if(messagePxSize >= MAX_PX) {
                toSendAfter = recentColorCode + message.substring(lastSpaceIndex + 1, message.length());
                message = message.substring(0, lastSpaceIndex + 1);
                break;
            }

            charIndex++;
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate = CENTER_PX - halvedMessageSize;
        int spaceLength = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated = 0;
        StringBuilder stringBuilder = new StringBuilder();

        while (compensated < toCompensate) {
            stringBuilder.append(" ");
            compensated += spaceLength;
        }

        player.sendMessage(stringBuilder.toString() + message);

        if(toSendAfter != null) {
            sendCenteredMessageV2(player, toSendAfter);
        }
    }

    @Override
    public String translateString(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    @Override
    public void sendMessages(Player player, List<String> list) {
        for(String s : list) {
            player.sendMessage(translateString(s));
        }
    }

}
