package net.aminecraftdev.custombosses.zcore.c;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Created by charl on 05-Apr-17.
 */
public enum Debug {

    PREFIX("&b[CustomBosses] &f"),
    MCUPDATE_LOADED("MCUpdate has been successfully set up!"),
    MCUPDATE_FAILED("MCUpdate failed to initialise a connection to the database."),
    NMS_METHODS("Using {0} methods."),

    ActionBar_NullPlayer("The specified player for the ActionBar task is null!"),
    ActionBar_NullMessage("The specified message for the ActionBar task is null!"),

    ChunkReflection_NullLocation("The specified location is null!"),
    ChunkReflection_NullWorld("The specified world is null!"),

    AutoSpawn_LoadingPlaceholders("Loading all Placeholders (if enabled) and AutoSpawn sections."),
    AutoSpawn_Disabled("AutoSpawn feature has been disabled."),
    AutoSpawn_NullKey("The AutoSpawn Key is null!"),
    AutoSpawn_MapDoesntContainKey("The AutoSpawn map doesn't contain the key: '{0}'"),
    AutoSpawn_InvalidBoss("The specified boss: '{0}' is not a valid boss section! (AutoSpawn section: '{1}')"),
    AutoSpawn_WorldNull("The world for the AutoSpawn section: '{0}' is null or doesn't exist!"),
    AutoSpawn_InvalidSpawnRate("The spawn rate for the AutoSpawn section: '{0}' is not valid!"),

    Boss_NotLatestDropTable("The Boss section: '{0}' is not setup to the latest version of CustomBosses which contains DropTables."),
    Boss_DeleteOldFiles("  Please delete your old CustomBosses folder/files and reload/restart the server to generate new files."),
    Boss_NotUsingDropTable("Please remember that without using drop tables the drops will only be limited to the top damager."),
    Boss_InvalidEntityType("The bossType: '{0}' is not a valid EntityType!"),
    Boss_NullName("Boss name is null therefore the boss couldn't be spawned."),
    Boss_OverMaxHealth("The bosses health cannot be greater than 2000! (Attempted to set health to: '{0}')"),
    Boss_NullPotion("The potion effect type '{0}' is invalid! Please use a valid potion effect type! All valid types: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/potion/PotionEffectType.html"),

    Skill_Null("The specified skill is null!"),
    Skill_Empty("You cannot leave the skill as empty!"),
    Skill_Command_EmptyList("The command list for the skill: '{0}' is empty!"),
    Skill_Message_EmptyList("The message list for the skill: '{0}' is empty!"),
    Skill_Custom_Null("The specified custom skill type is null for skill: '{0}'!"),
    Skill_Custom_Empty("The specified custom skill type is empty for skill: '{0}'!"),
    Skill_Custom_BaseNotFound("The skillType for the skill: '{0}' could not be found!"),
    Skill_Type_NotFound("The skill type isn't found for the skill: '{0}'"),
    Skill_Type_DoesntExist("The skill type '{0}' doesn't exist for the skill: '{1}'"),

    HolographicDisplay_PlaceholderLoaded("-> Placeholder '{0}' is now loaded and bound to AutoSpawn section: '{1}'!"),

    DropTable_DoesntContain("The droptable.yml doesn't contain the DropTable: '{0}' that the plugin is trying to look for."),
    DropTable_InvalidDamagePosition("'{0}' is not a valid damage position, damage positions must be a positive number, for example 5."),
    DropTable_InvalidChance("'{0}' is not a valid chance! The chance must be a positive number!");

    private String _prefix = null;
    private String _message;

    Debug(String message) {
        _message = message;
    }

    public String getMessage() {
        return _message;
    }

    public void out(Object... objects) {
        if(_prefix == null) {
            _prefix = Debug.PREFIX.getMessage();
        }

        String r = _message;
        int i = 0;

        for(Object object : objects) {
            String placeholder = "{" + i + "}";
            if(r.contains(placeholder)) {
                r = r.replace(placeholder, ""+object);
                i++;
            }
        }

        Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', _prefix + r));
    }


}
