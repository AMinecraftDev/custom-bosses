package net.aminecraftdev.custombosses.zcore.c;

import net.aminecraftdev.custombosses.zcore.i.IItemStackUtils;
import org.apache.commons.lang.math.*;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Charles on 7/3/2017.
 */
public class ItemStackUtils implements IItemStackUtils {

    private NumberUtils numberUtils = new NumberUtils();

    @Override
    public ItemStack createItemStack(ConfigurationSection configurationSection) {
        return createItemStack(configurationSection, 1, null);
    }

    @Override
    public ItemStack createItemStackAndAmount(ConfigurationSection configurationSection) {
        int amount = configurationSection.getInt("amount");

        return createItemStack(configurationSection, amount, null);
    }

    @Override
    public ItemStack createItemStack(ConfigurationSection configurationSection, int amount, Map<String, String> replacedMap) {
        String type = configurationSection.getString("type");
        String name = configurationSection.getString("name");
        List<String> lore = configurationSection.getStringList("lore");
        List<String> enchants = configurationSection.getStringList("enchants");
        short durability = (short) configurationSection.getInt("durability");
        String owner = configurationSection.getString("owner");
        String spawner = configurationSection.getString("spawner");
        Map<Enchantment, Integer> map = new HashMap<>();
        List<String> newLore = new ArrayList<>();
        Material mat = getType(type);
        short meta = 0;
        boolean addGlow = false;

        if(type instanceof String) {
            String sType = (String) type;

            if(sType.contains(":")) {
                String[] split = sType.split(":");

                meta = Short.valueOf(split[1]);
            }
        }

        if((replacedMap != null) && (name != null)) {
            for(String z : replacedMap.keySet()) {
                if(!name.contains(z)) continue;

                name = name.replace(z, replacedMap.get(z));
            }
        }

        if(lore != null) {
            for(String z : lore) {
                String y = z;

                if(replacedMap != null) {
                    for(String x : replacedMap.keySet()) {
                        if(!y.contains(x)) continue;

                        y = y.replace(x, replacedMap.get(x));
                    }
                }

                if(y.contains("\n")) {
                    String[] split = y.split("\n");

                    for(String s2 : split) {
                        newLore.add(ChatColor.translateAlternateColorCodes('&', s2));
                    }
                } else {
                    newLore.add(ChatColor.translateAlternateColorCodes('&', y));
                }
            }
        }

        if(enchants != null) {
            for(String s : enchants) {
                String[] spl = s.split(":");
                String ench = spl[0];

                if(ench.equalsIgnoreCase("SWEEPING_EDGE")) {
                    int level = Integer.parseInt(spl[1]);

                    map.put(Enchantment.SWEEPING_EDGE, level);
                } else if(ench.equalsIgnoreCase("GLOW")) {
                    addGlow = true;
                } else {
                    int level = Integer.parseInt(spl[1]);

                    map.put(Enchantment.getByName(ench), level);
                }
            }
        }

        ItemStack itemStack = new ItemStack(mat, amount, meta);
        ItemMeta itemMeta = itemStack.getItemMeta();

        if(!newLore.isEmpty()) {
            itemMeta.setLore(newLore);
        }
        if(name != null) {
            if(!name.equals("")) {
                itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            }
        }

        itemStack.setItemMeta(itemMeta);

        if(!map.isEmpty()) {
            itemStack.addUnsafeEnchantments(map);
        }
        if(configurationSection.contains("durability")) {
            short dura = itemStack.getType().getMaxDurability();
            dura -= (short) durability - 1;

            itemStack.setDurability(dura);
        }

        if(configurationSection.contains("owner") && itemStack.getType() == Material.SKULL_ITEM) {
            SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();

            skullMeta.setOwner(owner);

            itemStack.setItemMeta(skullMeta);
        }

        if(configurationSection.contains("spawner") && itemStack.getType() == Material.MOB_SPAWNER) {
            BlockStateMeta blockStateMeta = (BlockStateMeta) itemStack.getItemMeta();
            BlockState blockState = blockStateMeta.getBlockState();
            CreatureSpawner creatureSpawner = (CreatureSpawner) blockState;

            if(this.numberUtils.isStringInteger(spawner)) {
                int number = Integer.valueOf(spawner);

                creatureSpawner.setSpawnedType(EntityType.fromId(number));
                blockStateMeta.setBlockState(blockState);
                itemStack.setItemMeta(blockStateMeta);
            }
        }

        return addGlow? addGlow(itemStack) : itemStack;
    }

    @Override
    public Material getType(String string) {
        Material material = Material.getMaterial(string);

        if(material == null) {
            if(org.apache.commons.lang.math.NumberUtils.isNumber(string)) {
                material = Material.getMaterial(Integer.valueOf(string));

                if(material != null) return material;

                return null;
            } else {
                String[] split = string.split(":");

                material = Material.getMaterial(Integer.valueOf(split[0]));

                if(material != null) return material;

                return null;
            }
        }

        return material;
    }

    public static ItemStack addGlow(ItemStack itemStack) {
        return GlowEnchant.addGlow(itemStack);
    }

}
