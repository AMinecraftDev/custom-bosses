package net.aminecraftdev.custombosses.zcore.i;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IReflectionUtils {

    String getAPIVersion();

    Class<?> getNMSClass(String name);

    Class<?> getOBCClass(String name);
}
