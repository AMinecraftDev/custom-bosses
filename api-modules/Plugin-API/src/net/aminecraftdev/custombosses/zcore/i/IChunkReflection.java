package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.Location;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IChunkReflection {

    Object getChunkProviderServer(Location location);

    boolean isChunkLoaded(Location location);

    void loadChunk(Location location);
}
