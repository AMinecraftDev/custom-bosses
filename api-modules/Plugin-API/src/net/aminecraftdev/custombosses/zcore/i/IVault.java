package net.aminecraftdev.custombosses.zcore.i;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IVault {

    boolean setupEconomy();

    boolean setupChat();

    boolean setupPermission();

    Economy getEconomy();

    Chat getChat();

    Permission getPermission();

}
