package net.aminecraftdev.custombosses.zcore.i;

/**
 * Created by Charles on 7/3/2017.
 */
public interface INumberUtils {
    String formatDouble(double d);

    String formatTime(int time);

    boolean isStringInteger(String s);

    boolean isStringDouble(String s);
}
