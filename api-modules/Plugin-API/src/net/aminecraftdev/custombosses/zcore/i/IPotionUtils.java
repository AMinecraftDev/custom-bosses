package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.potion.PotionEffect;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IPotionUtils {

    PotionEffect getPotionEffect(ConfigurationSection configurationSection);
}
