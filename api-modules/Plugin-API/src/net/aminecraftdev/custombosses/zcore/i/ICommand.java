package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created by Charles on 7/3/2017.
 */
public interface ICommand extends CommandExecutor {

    boolean onCommand(CommandSender sender, Command cmd, String label, String[] args);

}
