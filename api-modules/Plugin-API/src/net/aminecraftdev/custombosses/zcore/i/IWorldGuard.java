package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.Location;

import java.util.List;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IWorldGuard {

    boolean isPvPAllowed(Location loc);

    boolean isBreakAllowed(Location loc);

    boolean allowsExplosions(Location loc);

    List<String> getRegionNames(Location loc);

    boolean isMobSpawningAllowed(Location loc);

}
