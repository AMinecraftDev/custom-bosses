package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IActionBarReflection {

    Plugin getPlugin();

    Object getPlayerConnection(Player player);

    void sendActionBar(Player player, String message);

    void sendActionBar(Player player, String message, int duration);

    void sendActionBarToAllPlayers(String message);

    void sendActionBarToAllPlayers(String message, int duration);
}
