package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IItemStackUtils {

    ItemStack createItemStack(ConfigurationSection configurationSection);

    ItemStack createItemStackAndAmount(ConfigurationSection configurationSection);

    ItemStack createItemStack(ConfigurationSection configurationSection, int amount, Map<String, String> replaced);

    Material getType(String string);
}
