package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.inventory.Inventory;

import java.util.HashMap;

/**
 * Created by Charles on 7/3/2017.
 */
public interface IInventoryUtils {
    void createInventory();

    Inventory getInventory();

    void addReplacedMap(HashMap<String, String> map);
}
