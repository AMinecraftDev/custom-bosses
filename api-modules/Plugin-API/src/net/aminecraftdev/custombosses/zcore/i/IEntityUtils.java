package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

/**
 * Created by charl on 4/3/2017.
 */
public interface IEntityUtils {

    LivingEntity getWitherSkeleton(Location location);

    LivingEntity getElderGuardian(Location location);

    LivingEntity getKillerBunny(Location location);

    LivingEntity getBabyZombie(Location location);

    LivingEntity getZombie(Location location);

    LivingEntity getBabyPigZombie(Location location);

    LivingEntity getPigZombie(Location location);

    LivingEntity getSlime(Location location, int size);

    LivingEntity getMagmaCube(Location location, int size);

    LivingEntity getChargedCreeper(Location location);
}
