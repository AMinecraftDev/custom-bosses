package net.aminecraftdev.custombosses.zcore.i;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Charles on 20/1/2017.
 */
public interface IMessageUtils {

    int getMaxPixel();

    List<String> getCenteredMessage(String message);

    void sendCenteredMessageV1(CommandSender player, String message);

    void sendCenteredMessageV2(CommandSender player, String message);

    String translateString(String message);

    void sendMessages(Player player, List<String> list);
}
